---
title: "npm for Bitbucket 2.0: Now with private packages! "
date: "2016-05-23T06:00:00+07:00"
author: "ssmith"
description: "The npm integration for Bitbucket now supports private packages."
categories: ["npm", "bitbucket", "atlassian-connect"]
---

<style>
  .float-image {
      display: block; 
      margin: 15px auto 30px auto;
      box-shadow: 10px 10px 15px #888888;
  }
</style>

We're pleased to announce version 2.0 of [Tim's][tpettersen]
[npm for Bitbucket integration][bbnpm-blog]. The major change for this release
is the addition of support for private packages. Read on for more information,
plus a little easter-egg.

## Private repositories

The headline feature for this release is the addition of support for private
packages in the [official npm repository][npmjs]. If you have private packages,
you just need to add your [authentication token from your `.npmrc`][npmrc] in
your Bitbucket settings. To do this go to `Bitbucket Settings->Configure NPM
Stats`:

  <img class='float-image' src='auth-key.png'>

Once that is in place you can then visit any Bitbucket npm project with a
private package and you will be able to see metadata for it:

  <img class='float-image' src='metadata-only.png'>

One thing to note is that we display slightly different data here, as private
packages have different stats stored against them. Private packages now show
maintainer and dependency information, instead of downloads. But don't worry;
public packages carry on working the same as before.

As with the previous version, clicking on the metadata will bring you to a
custom `package.json` file viewer that analyses the package data and displays a
table of dependencies with current and latest versions:

  <img class='float-image' src='file-metadata.png'>

But to complement this we've also added something new...

## A small easter-egg

You may notice above in the viewer dropdown there is a third viewer, called
`Dependency visualisation`. When viewing the `package.json` you can select this
option to open up a basic visualisation of your package dependencies:

  <img class='float-image' src='file-vis.png'>

This visualisation is also available by clicking on the dependencies count on
the main metadata view in your Bitbucket repository.

## Getting it now

Version 2.0 of the add-on is currently going through the process of being updated
in the Bitbucket marketplace, however if you want to get it early just follow
these steps:

1. Go to `Bitbucket Settings -> Manage Integrations` and remove the current npm
   add-on if necessary.
1. Click on `Install add-on from URL`.
1. Enter <https://bitbucket-npm.useast.atlassian.io/atlassian-connect.json>

That's all; after that, and setting up your authentication token, your private
(or public) packages should now show the updated metadata and data
visualisation. Have fun!



[bbnpm-blog]: https://developer.atlassian.com/blog/2016/01/bitbucket-npm-integration/
[tpettersen]: https://developer.atlassian.com/blog/authors/tpettersen/
[npmjs]: https://www.npmjs.com/
[npmrc]: https://www.npmjs.com/settings/tokens
