---
title: "JIRA 7.2.0 EAP1 Now Available"
date: "2016-06-07T16:00:00+07:00"
author: "shaffenden"
categories: ["jira", "aui", "apis", "eap"]
series: ["JIRA Startup Guides"]
lede: "The JIRA development team is proud to announce
       that the JIRA 7.2 Early Access Program (EAP)
       release downloads are now available."
---

The JIRA development team is proud to announce that the JIRA 7.2 Early Access
Program (EAP) release downloads are now available. We've prepared separate EAP
downloads for JIRA Software and JIRA Core, and they include important
functional changes such as the Atlassian User Interface (AUI) upgrade, and
updates to the project sidebar and navigation.
[Download the JIRA 7.2 EAP today][download-eap],
and check out the
[preparing for JIRA 7.2 development guide][preparing-for-jira-7.2]
to start testing your add-ons for compatibility.

<img style="width:60%;display:block;margin:16px auto" src="atlassian_AUIsidebar.png" />

## What's the deal with EAPs?

[EAP releases][eap-releases] are early previews of JIRA during development. We
release these previews so that Atlassian's add-on developers can see new
features as they are developed and test their add-ons for compatibility with
new APIs.

The number of EAP releases before the final build varies depending on the
nature of the release. The first publicly available EAP release for JIRA 7.2
is EAP20160530074712. Developers should expect that we will only provide a few
EAP releases before the final RC release.

<img style="width:60%;display:block;margin:16px auto" src="atlassian_JIRA.png" />

The JIRA 7.2 EAP is best explored hand-in-hand with the
[Preparing for JIRA 7.2 development guide][preparing-for-jira-7.2].
The development guide contains all changes in JIRA 7.2, and important new
features that may affect add-on developers. If you discover a bug or have
feedback on JIRA 7.2, **please let us know.**
[Create an issue in the JIRA project on jira.atlassian.com][create-issue]
with the component JIRA 7.2 EAP.

<div style="text-align:center;margin: 5% auto"><button class="aui-button aui-button-primary"><a style="color:white" href="https://www.atlassian.com/software/jira/download-eap?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement">Download the JIRA 7.2 EAP</a></button></div>

[download-eap]: https://www.atlassian.com/software/jira/download-eap
[preparing-for-jira-7.2]: https://developer.atlassian.com/display/JIRADEV/Preparing+for+JIRA+7.2
[eap-releases]: https://confluence.atlassian.com/display/JIRA/EAP+Releases?utm_source=DAC&utm_medium=blog&utm_campaign=jira-eap-announcement
[create-issue]: https://jira.atlassian.com/secure/CreateIssue!default.jspa?selectedProjectId=10240
