---
title: "Insights on Women in Tech"
date: "2016-03-30T16:00:00+07:00"
author: "cmountford"
description: "Denise Unterwurzacher shares some surprising stories about her
    experience as a woman in tech based on my interview with her for Atlassian Tech
    TV" 
categories: ["video", "women", "diversity"] 
---

# Insights on Women in Tech

In a [recent interview][denise-interview] with Confluence developer [Denise
Unterwurzacher][denise-interview] for [Atlassian Tech TV][tech-tv-channel], in
addition to discussing the role of technical support in Atlassian and her move
from that role to software development, we also tackled the topic of women in
tech. In all honesty, I was nervous even asking her about it because while it's
an important topic, I didn't want to put her on the spot and suggest that just
because she is a woman in tech that she would want to dig into the subject on
camera. As it turned out, Denise was not only keen to share her experiences,
but she also provided some fascinating insights and a couple of stories that
completely surprised me.

<center style="margin-top:18px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/ybYGXmRF4cs" frameborder="0" allowfullscreen></iframe></center> 

As the technology industry faces up to its lopsided stats and seeks to address
its terrible lack of diversity with initiatives and awareness programs, there
is a lot that individuals can do &mdash; men, women, and people who identify
with less traditional binary sex and gender definitions. As with many
companies, Atlassian has initiatives in place to attract and retain qualified
women in engineering positions to address the imbalanced ratio of men and women
in tech. 

What can I do as a white, heterosexual, [cisgender][cis] man? Aren't I the
problem? No. I'm not the problem, it's attitudes and behaviors that are the
problem, and like anyone else, I need to work and learn to overcome unconscious
bias. As a part of this work, it's important to find out more about other
people's perspectives. My interview with Denise uncovered some interesting new
angles I hadn't considered.

[cis]: https://en.wikipedia.org/wiki/Cisgender

## One of the boys

Some women are happy being "one of the boys" and Denise confesses she is one of
these women. But, Denise explains, many other women need to feel less like
an outsider in order to enter or stay in a technology career. It's not
necessary or helpful for men to feel threatened by this fact. There are a range
of perspectives and experiences. Something I hadn't considered was the role
those women play who are comfortable being in the minority, the women who might
be more likely to find themselves as the only woman on a team.

> I was used to being the only girl in the entire team, just _one of the boys_,
> which is fine. That works for some people and it doesn't work for others. I
> was actually talking with another girl at Atlassian recently and she
> mentioned that even though she's happy being one of the boys, maybe it
> actually makes other women less comfortable to even attempt to join
> technology. So she sees it as part of her responsibility to be welcoming to
> the other women as well. I think it's very important and it's something that
> a lot of women forget about or just aren't really aware of.

## I am the woman around here 

Recalling her earlier years in system administrator roles at other companies,
Denise tells a surprising story she's seen play out, saying that there are
times when "women in tech can be their own worst enemies." 

> There's often one woman and she's used to being the one woman in the team and
> if you come in as another woman, I've actually had a couple of women be very
> unwelcoming and very hostile. "I am the one of the boys and you're not
> invited!"

It's worth emphasizing that Denise's experience won't match that of every woman
and may not even be typical but it sure gives us some insight into the details
that help us appreciate diversity.

![](silhouette.png)

While it's understandable that the industry narrative challenges men to
practice more inclusive behaviors as a part of making the next waves of women
feel welcomed into technology roles, Denise also encourages women to adopt the
same attitude, highlighting the importance of the way women already in tech
behave.

## Bias and privilege

I've noticed a tendency towards denial that there is a problem, both in my own
knee-jerk reactions and in hasty responses by other well-meaning men when faced
by the fact as Denise puts it that many women are always "made to feel
hyper-aware of their difference." Not being conscious of what we do that
contributes to people feeling alienated may make us think that there is no
change we should make in ourselves. There are two problems with this. The first
is that because unconscious bias is unconscious, we should not expect to detect
its presence without awareness training. The second problem is that too
frequently we can't avoid being caught up in self-righteousness, fueled by the
pride of our self-image as a reasonable, educated and civilized human. We
hijack the agenda with unwarranted outrage when what's needed is empathy and
focus on opportunities to help each other.

![Atlassian Developer Symphony Wong](symph.png "Atlassian Developer Symphony
Wong features in an upcoming Atlassian Tech TV episode")

Men I've talked to can become too easily stuck on the unpalatable idea that
they might be contributing to the problem. To help prevent being derailed by
paralyzing indignation, it's helpful to stop seeing it as women vs men battle
lines. Not only men suffer from unconscious bias.

> Everybody has bias and everybody has privilege &mdash; it's very difficult
> to be without all of those things. You just have to be aware of it. When it
> does come up if someone makes you aware of something that you've done, you
> just have to step outside yourself for a moment and understand where it comes
> from.

Being a man in tech includes certain privileges such as being invisibly free
from that experience of being an outsider &mdash; the notable exception being
women in tech events which have become more prevalent in recent years. Denise
confirms my experience that women generally welcome participation and help from
men in these events, but obviously, at some point, if there are too many men in
attendance, it ceases to be a women in tech event. 

Some people question whether women in tech events are the right path. Clearly
they're a response to the need for change. Denise reports that some of her
friends suggested that this is a kind of segregation that makes a bigger
problem but Denise does not agree.

> I think that's not true. There is a problem already. There is a problem with
> the representation of women in tech. That's not a secret. It hasn't come
> about because of women in technology events. Women in technology events don't
> make it worse, and not having them &mdash; ignoring it &mdash; is not going
> to make it better. This problem isn't going to go away on its own.

![](denise.png "Confluence Developer Denise Unterwurzacher")

Unconscious bias is something I assume to be at work in me as it is in
everyone. Do I encourage my son in his interest in robotics more than I
encourage my two daughters to develop technology skills like coding and
electronics? Taking control of technology and being tech literate is useful
even for someone not interested in a technology career. 

I'm not sure what being "in tech" will even mean by the time my kids finish
school but if we get Denise's way, my daughters wouldn't be thinking about
being "women in tech" any more than I do today about "what it's like being a
man in tech".

> I really hope that in ten years, twenty years, I don't know how long, but I
> desperately hope that some day people look back and wonder why on earth we
> had women in tech events because it just seems absurd. But we're not there
> yet.

Subscribe to [Atlassian Tech TV][tech-tv-channel] to make sure you catch
upcoming episodes and while Denise doesn't tweet you should watch [the first
part of my interview with Denise][denise-part-1] to hear her thoughts on technical
support culture and paths to careers in technology and you can follow me, Chris 
Mountford on Twitter: [@chromosundrift][ctwitter]

[denise-interview]: https://youtu.be/ybYGXmRF4cs

[denise-part-1]: https://youtu.be/3dsB2DbrDWA 

[tech-tv-channel]: https://www.youtube.com/c/AtlassianTechTV

[ctwitter]: https://twitter.com/chromosundrift

