---
title: "Last chance to register for AtlasCamp 2015!"
date: "2015-05-25T09:00:00+07:00"
author: "nwade"
categories: ["events", "atlascamp", "git"]
---

<img class="right-image" style="width: 300px;" alt="AtlasCamp" src="https://www.atlassian.com/wac/company/about/events/atlascamp/2015/sectionWrap/0/column/0/imageBinary/atlascamp-logo.svg"/>
Once a year developers from all over the world get together for Atlassian's premier 
developer conference: [AtlasCamp]. You can learn how to customize and integrate 
JIRA, Confluence, Bitbucket, HipChat, Bamboo, and Stash, helping your team work 
even faster. And if you want to take your developer skills to the next level with 
Git, CI, CD, Docker and more, you should be at [AtlasCamp]. 
 
There's only 14 days left until AtlasCamp, register now before we run out of spots:
<div class="signup-container"><a class="aui-button aui-button-primary" href="http://atlascamp.com">Secure my spot at AtlasCamp</a></div>
 
## Why should you come to AtlasCamp?
There's many great reasons, but we boiled it down to four things you can take away. 

1. **Go Faster** - Help your team work smarter by learning how to customize Atlassian 
products to suit your work, and your tools.  

2. **Knowledge** - Become an expert with developer tools like Git and Docker, and 
spend more time doing what you love; making software.

3. **Networking** - Meet like-minded developers, chat with AWS, Google, and more PaaS 
providers, and have a blast at the attendee party.

4. **Opportunity** - See how other developers have built and sold add-ons in the Atlassian 
Marketplace, find your own niche, and get training how you can too. 
 
We look forward to hosting you at AtlasCamp in Prague on June 9-11.

There's only 14 days left. So don't wait. Register now. 
<div class="signup-container"><a class="aui-button aui-button-primary" href="http://atlascamp.com">Secure my spot at AtlasCamp</a></div>

[AtlasCamp]: https://www.atlassian.com/atlascamp?utm_source=DAC&utm_medium=Blog&utm_campaign=LastChance

<style>
  .right-image {
    float: right;
	margin: 1em;
  }
  a.aui-button.aui-button-primary {
    font-size:16px;
    line-height:24px;
  }
</style>
