#!/bin/bash
# This is a test script to quickly create a project in JIRA Software.
# And add a certain number of issues in the process.
#
#--------------------------------------------
# Author: Peter Van de Voorde
# Email: pvandevoorde@atlassian.com
# Date: 1st of December 2015
#---------------------------------------------

# Let's start with counting the number of Arguments provided.
# We need: User, Password, JIRA Software base url, Project Name, desired number of issues

if (( $# != 6 )); then
    echo "You need the following parameters:"
    echo "1. Username"
    echo "2. Password"
    echo "3. JIRA Software base url"
    echo "4. Project name"
    echo "5. Project Key"
    echo "6. Desired number of issues"
    echo ""
    echo "Run the script like this: ./set-up-test-project.sh admin 1234 http://test.baseurl.com 'Test Project' TEST 10"
    exit 0
fi

user=$1
pwd=$2
baseurl=$3
project_name=$4
project_key=$5
issues=$6

#Let's create our project and get the project id back for later
project_id="$(curl -u ${user}:${pwd} -H "Content-Type: application/json" -X POST -d '{
    "key": "'"${project_key}"'",
    "name": "'"${project_name}"'",
    "projectTypeKey": "Software",
    "projectTemplateKey": "com.pyxis.greenhopper.jira:gh-scrum-template",
    "description": "Test Project",
    "lead": "'"${user}"'",
    "url": "https://developer.atlassian.com",
    "assigneeType": "PROJECT_LEAD"
}' ${baseurl}/rest/api/2/project | sed 's/{.*id":"*\([0-9]*\)"*,*.*}/\1/')"

#Let's add a some issues
for ((n=1;n<=$issues;n++))
do
  curl -s -u ${user}:${pwd} -H "Content-Type: application/json" -X POST -d '
  {
      "fields": {
          "project": {
              "id": "'"${project_id}"'"
          },
          "summary": "'"${project_name}"' task '"${n}"'",
          "issuetype": {
              "id": "10002"
          },
          "assignee": {
              "name": "'"${user}"'"
          },
          "reporter": {
              "name": "'"${user}"'"
          },
          "priority": {
              "id": "1"
          },
          "description": "This is an example task created through the REST API."
      }
  }' ${baseurl}/rest/api/2/issue/ >> /dev/null
done

echo Project ${project_name} has been created with ${issues} issues.
