---
title: "Tip of The Week - Adapting the Issue Collector"
date: "2015-09-09T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","JIRA"]
lede: "The issue collector is one of the many hidden gems in JIRA.
It's easy to use and can provide you with another tool to get feedback from your users into JIRA."
---
After a short break we're back with more Tip of The Week articles. This week's article is about the JIRA issue collector and how to remove the default name and email field. Because of restrictions on JIRA Cloud this tip only works on JIRA Server.

The [issue collector](https://confluence.atlassian.com/jira/using-the-issue-collector-288657654.html?utm_source=dac&utm_medium=blog&utm_campaign=totw "issue collector") is one of the many hidden gems in [JIRA](https://www.atlassian.com/software/jira?utm_source=dac&utm_medium=blog&utm_campaign=totw "JIRA").
It's easy to use and can provide you with another tool to get feedback from your users into JIRA.

The issue collector does have 2 fields that are not configurable: Name and Email. They're shown on the collector form when the user isn't logged in to JIRA.
These fields are not mapped on custom fields, but are simply shown in the description field of the issue.
So how can you remove these fields and replace them with your own custom ones?

Just add the following to the description of a field on your issue collector in your project's field configuration scheme:

    <script type="text/javascript">
        jQuery(document).ready(function() {
        jQuery(".contact-form-fields.field-group").hide();
        });
    </script>


Now you can use your own custom fields for Name and Email and make them searchable in JIRA.
