---
title: "Docker machine screencast: deploy Atlassian everywhere!"
date: "2015-02-25T13:00:00+07:00"
author: "npaolucci"
categories: ["docker", "stash", "devops"]
---

Even if it's still in early stages of development, [Docker Machine][16] is a
very powerful tool, one of the three very intriguing new pieces of the Docker
ecosystem - the other ones being [compose][19] and [swarm][20]. What does
Docker machine do? It allows you to create and manage Docker hosts on local
machines or cloud providers. 

Since we recently released an [official docker image][15] for our enterprise
[Git repository manager Stash][18] - a first step towards dockerizing all our
products - what better opportunity than to show the basics of docker machine
and at the same time show how easy to deploy Stash?

Because the written word is sometimes dry and my voice is much more
enthusiastic than my text I thought this time I'd whip up a quick demo video to
show you the capability of Docker machine and show you how easy it is to use it
to deploy your applications.

Enjoy and let us know what you think at [@atlassiandev][7] or at [@durdn][6].

### Screencast

<iframe width="850" height="500" src="https://www.youtube.com/embed/zhAT-gZcpBM" frameborder="0" allowfullscreen></iframe>

[6]: http://twitter.com/durdn
[7]: http://twitter.com/atlassiandev
[15]: http://blogs.atlassian.com/2015/01/stash-docker/
[16]: https://github.com/docker/machine
[18]: https://www.atlassian.com/software/stash
[19]: https://github.com/docker/fig
[20]: https://github.com/docker/swarm
