---
title: "Tip of the week: securing your Bamboo remote agents"
date: "2015-07-09T18:00:00+07:00"
author: "jgarcia"
categories: ["totw","Bamboo"]
---

This week's tip comes from the Bamboo documentation, with some extra explanation
provided by Rafael from the Bamboo support team. We always recommend taking the
steps to secure publicly-accessible hosts to prevent unauthorized access or
packet capture.

The best way to secure the Bamboo remote agents and their transmission channels
is the use of secure shell (otherwise known as SSH) for data encryption when
using insecure networks. Read on about [Securing your remote
agents](https://confluence.atlassian.com/display/BAMBOO/Securing+your+remote+agents)
in our documentation, then be sure to check out the extra notes found in the
[Bamboo Knowledge
Base](https://confluence.atlassian.com/pages/viewpage.action?pageId=757465762).

Let us know what
you think in the comments!

Watch out for news and info from our
[@AtlassianDev](https://www.twitter.com/atlassiandev) Twitter feed!

Follow me at [@bitbucketeer](https://www.twitter.com/bitbucketeer)!
