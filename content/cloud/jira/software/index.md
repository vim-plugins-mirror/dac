---
title: JIRA Software Cloud Development
platform: cloud
product: jswcloud
category: devguide
subcategory: index
aliases:
- /jiracloud/jira-software-cloud-development-39981104.html
- /jiracloud/jira-software-cloud-development-39981104.md
confluence_id: 39981104
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981104
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981104
date: "2016-05-24"
---
# JIRA Cloud : JIRA Software Cloud development

## What is JIRA Software Cloud?

JIRA Software Cloud helps software teams plan, track, and release great software. It's a hosted service from Atlassian for agile software development, with features sprint planning, scrum and kanban boards, agile reports, dashboards, and more. You can also integrate JIRA Software Cloud with source code management and continuous integration tools to streamline your development processes. If you haven't used JIRA Software before, check out the <a href="https://www.atlassian.com/software/jira" class="external-link">product overview</a> for more information.

Thinking about building an add-on or integration for JIRA Software? JIRA Software is already used by thousands of companies, like NASA, Spotify, and Twitter; and there are over a thousand JIRA-related add-ons in the Atlassian marketplace. The opportunities for extending JIRA Software are endless!

## Integrating with JIRA Software

The three building blocks of integrating with JIRA Software are the REST API, web hooks, and web fragments.

![Alt text](../../../illustrations/atlassian-software-47.png)

### [JIRA Software REST API][JIRA Software Cloud REST API]

The JIRA Software REST API lets your add-on communicate with JIRA Software. For example, using the REST API, you can write a script that moves issues from one sprint to another.

JIRA Software is built on the JIRA platform, so you interact with it by using the JIRA Software REST API and JIRA platform REST API:

-   [JIRA Software REST API][JIRA Software Cloud REST API]
-   [JIRA platform REST API]

![Alt text](../../../illustrations/atlassian-software-46.png)

### [Webhooks][JIRA webhooks]

Webhooks notify your add-on or application when certain events occur in JIRA Software. For example, you could create a webhook that alerts your application when a sprint has ended.

JIRA Software allows you to register webhooks for events related to sprints, boards, and more. Check out the documentation for more details: [Webhooks].

![Alt text](../../../illustrations/atlassian-software-52.png)

### [JIRA Software modules][JIRA Software modules]

A module is simply a UI element, like a tab or a menu. JIRA Software UI modules allow add-ons to interact with the JIRA Software UI. For example, your add-on can use a JIRA Software UI module to add a new dropdown menu to a board.

For more information, see: [JIRA Software modules][JIRA Software modules].

## Build an add-on with Atlassian Connect

Atlassian Connect is the framework for building add-ons for Atlassian applications in the Cloud. If you want to know more about how the Atlassian Connect framework works, check out the [introduction to Atlassian Connect] *(Connect documentation),* otherwise get started below.
 

<a href="https://developer.atlassian.com/display/jiracloud/Getting+started" class="aui-button-primary aui-button">Start building add-ons</a>

Looking for inspiration? Here are a few examples of what you can build on top of JIRA Software Cloud:

-   <a href="https://marketplace.atlassian.com/plugins/com.sngtec.jira.cloud.kanbanalytics/cloud/overview" class="external-link">Retrospective Tools for JIRA</a> -- *This add-on provides a number of tools for reviewing your project history, and with the JIRA Software API they're able to let you filter by board, or even overlay your swimlanes.*
-   <a href="https://marketplace.atlassian.com/plugins/aptis.plugins.epicSumUp/cloud/overview" class="external-link">Epic Sum Up</a> -- *This add-on adds a panel to the view issue page that allows you to review your epic progress or every issue within the epic.*
-   <a href="https://marketplace.atlassian.com/plugins/com.tempoplugin.tempo-planner/cloud/overview" class="external-link">Tempo Planner for JIRA</a> -- *This add-on fetches information from boards, epics, and backlogs using the JIRA Software API, and lets you plan your work by team member.*

## Technical overview

JIRA Software is an [application] built on the JIRA platform. The JIRA platform provides a set of base functionality that is shared across all JIRA applications, like issues, workflows, search, email, and more. A JIRA application is an extension of the JIRA platform that provides specific functionality. For example, JIRA Software adds boards, reports, development tool integration, and other agile software development features.

This means that when you develop for JIRA Software, you are actually integrating with the JIRA Software application as well as the JIRA platform. The JIRA Software application and JIRA platform have their own REST APIs, web hook events, and web fragments. 

Read the overview of the **[JIRA Cloud platform development] **for more information.

## More information

-   **[JIRA Software Cloud tutorials]*** --* Learn more about JIRA Software development by trying one of our hands-on tutorials.
-   **<a href="https://answers.atlassian.com/questions/topics/753774/jira-agile-development" class="external-link">&quot;jira-agile-development&quot; tag</a> on the Atlassian Answers forum** -- Join the discussion on JIRA Software development.
-   **[Atlassian Connect documentation]** -- Find all the information you need about Atlassian Connect, including release notes on the latest changes.

  [JIRA Software Cloud REST API]: /jiracloud/jira-software-cloud-rest-api-39988028.html
  [JIRA platform REST API]: /cloud/jira/platform/jira-cloud-platform-rest-api
  [JIRA webhooks]: /cloud/jira/platform/webhooks
  [Webhooks]: https://developer.atlassian.com/display/JIRADEV/Webhooks
  [JIRA Software modules]: /cloud/jira/software/jira-software-modules
  [introduction to Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
  [application]: https://developer.atlassian.com/display/JIRADEV/JIRA+applications
  [JIRA Cloud platform development]: /cloud/jira/platform/jira-cloud-development-platform
  [JIRA Software Cloud tutorials]: /jiracloud/jira-software-tutorials-39988385.html
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/index.html
