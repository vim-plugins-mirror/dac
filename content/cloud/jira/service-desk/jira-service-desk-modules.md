---
title: JIRA Service Desk Modules 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-service-desk-modules-39988267.html
- /jiracloud/jira-service-desk-modules-39988267.md
confluence_id: 39988267
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988267
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988267
date: "2016-09-19"
---
# About JIRA Service Desk modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA platform modules can also be used to extend other parts of JIRA, like permissions and workflows.

The JIRA platform and JIRA Software also have their own modules. For more information, see the following pages:

-   [JIRA platform modules]
-   [JIRA Software modules]

## Using JIRA Service Desk modules

You can use a JIRA Service Desk module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `serviceDeskPortalHeaders` module to your add-on, which injects a panel at the top of customer portal pages in JIRA Service Desk.

**atlassian-connect.json**

``` js
...
"modules": {
    "serviceDeskPortalHeaders": [
        {
            "key": "sd-portal-header",
            "url": "/sd-portal-header"
        }
    ]
}
...
```

## JIRA Service Desk modules

-   [JIRA Service Desk modules - Agent view]
-   [JIRA Service Desk modules - Customer portal]
-   [JIRA Service Desk Automation Action module]

 

  [JIRA platform modules]: /cloud/jira/platform/jira-platform-modules
  [JIRA Software modules]: /cloud/jira/software/jira-software-modules
  [JIRA Service Desk modules - Agent view]: https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Agent+view
  [JIRA Service Desk modules - Customer portal]: https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+modules+-+Customer+portal
  [JIRA Service Desk Automation Action module]: https://developer.atlassian.com/display/jiracloud/JIRA+Service+Desk+Automation+Action+module
