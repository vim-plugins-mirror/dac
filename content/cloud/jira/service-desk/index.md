---
title: JIRA Service Desk Cloud Development
platform: cloud
product: jsdcloud
category: devguide
subcategory: index
aliases:
- /jiracloud/jira-service-desk-cloud-development-39981106.html
- /jiracloud/jira-service-desk-cloud-development-39981106.md
confluence_id: 39981106
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981106
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981106
date: "2016-09-20"
---
# JIRA Service Desk Cloud development

## What is JIRA Service Desk Cloud?

JIRA Service Desk is Atlassian's* *fastest growing product...and we're only getting started. With over 15,000 teams using it, we're excited about the opportunities for developers and IT teams, and system integrators to take advantage of our APIs.  

{{< youtube 5u62pSDVB_I >}} 


IT teams primarily use JIRA Service Desk for a service solution. This can range from asset management to DevOps (see the use cases below). IT teams are also diverse, including support desk teams, customer service teams, and operations teams, to name a few. JIRA Service Desk supports these teams and solutions with powerful features like customer request portals, support queues, SLAs, and more. If you haven't used JIRA Service Desk before, check out the <a href="https://www.atlassian.com/software/jira/service-desk" class="external-link">product overview</a> for more information.

### JIRA Service Desk use cases

-   **Support helpdesk**: An easy way to provide support to anyone in the organization with IT requests such as hardware (laptop) requests, or software requests. 
-   **ITSM/ITIL:** More advanced IT teams want to use a service solution to support ITSM and ITIL processes, including incident, problem, and change management. 
-   **Asset management:** IT teams want to discover, control, monitor, and track key IT assets such as hardware and servers. 
-   **DevOps: **Developer, Operations, and IT teams can use JIRA Service Desk to collaborate together and solve problems faster. 
-   **Business teams:** Finance and HR teams may want to use JIRA Service Desk to collect requests from anyone in the organization. 

## Integrating with JIRA Service Desk

The three building blocks of integrating with JIRA Service Desk are the REST API, web hooks, and web fragments.

![Alt text](../../../illustrations/atlassian-software-47.png)

### [JIRA Service Desk REST API][JIRA Service Desk REST API]

The JIRA Service Desk REST API lets your add-on communicate with JIRA Service Desk. For example, using the REST API, you can retrieve a queue's requests to display in your add-on or create requests from phone calls.

JIRA Service Desk is built on the JIRA platform, so you interact with it by using the JIRA Service Desk REST API and JIRA platform REST API:

-   [JIRA Service Desk REST API][JIRA Service Desk REST API]
-   [JIRA platform REST API]

![Alt text](../../../illustrations/atlassian-software-46.png)

### [Automation rules and webhooks][JIRA Service Desk Webhooks]

Add-ons and applications can react to conditions/events in JIRA Service Desk via automation rules. You can implement an "automation action" that performs actions in a remote system as part of an automation rule. An automation rule can also be configured to fire a webhooks that notifies your add-on or application.

For more information, see [JIRA Service Desk webhooks][JIRA Service Desk Webhooks].

![Alt text](../../../illustrations/atlassian-software-52.png)

### [JIRA Service Desk modules][JIRA Service Desk Modules]

A module is simply a UI element, like a tab or a menu. JIRA Service Desk UI modules allow add-ons to interact with the JIRA Service Desk UI. For example, your add-on can use a JIRA Service Desk UI module to add a panel to the top of customer portals.

For more information, see: [JIRA Service Desk modules][JIRA Service Desk Modules].

#### Build an add-on with Atlassian Connect

Atlassian Connect is the framework for building add-ons for Atlassian applications in the Cloud. If you want to know more about how the Atlassian Connect framework works, check out the [introduction to Atlassian Connect] *(Connect documentation),* otherwise get started below.

``` xml
<div style="text-align:center"><a class="aui-button aui-button-primary" href="https://developer.atlassian.com/display/jiracloud/Getting+started" style="text-align:center;color:#ffffff;font-size:16px;">Start building add-ons</a></div>
```

### Looking for inspiration?

Here are a few examples of what you can build on top of JIRA Service Desk:

#### Customer portal customization

JIRA Service Desk provides an intuitive customer portal that makes it easy for non-technical end users to interact with service teams like IT and support. By extending this, you can build a completely tailored interface for the customer portal that matches your company's branding.

#### Collect requests outside of JIRA Service Desk

Build functionality to create requests on behalf of customers in any number of ways. For example, integrate it into the support section of your website, or have a get help menu on your mobile app, or hook up alerts from a system monitoring tool to create incidents in JIRA Service Desk.

#### SLA integration

JIRA Service Desk introduces the notion of service level agreements (SLAs) by letting teams accurately measure and set goals based on time metrics, e.g. time to assign, time to respond, time to resolution. With the JIRA Service Desk REST API, you can now get detailed SLA information and create your own reports. 

#### Telephony integration

Create requests based on incoming voice calls by integrating your telephony system with JIRA Service Desk, via the REST API.

#### Supplement request information

Add information about assets, the customer, or other relevant information to requests to make it easier for agents to solve problems and close requests.

 

## Technical overview

JIRA Service Desk is an application built on the JIRA platform. The JIRA platform provides a set of base functionality that is shared across all JIRA applications, like issues, workflows, search, email, and more. A JIRA application is an extension of the JIRA platform that provides specific functionality. For example, JIRA Service Desk adds customer request portals, support queues, SLAs, a knowledge base, and automation. 

This means that when you develop for JIRA Service Desk, you are actually integrating with the JIRA Service Desk application as well as the JIRA platform. The JIRA Service Desk application and JIRA platform each have their own REST APIs, web hook events, and web fragments. 

Read the overview of the **[JIRA Cloud platform development] **for more information.

## More information

-   **[JIRA Service Desk tutorials]*** --* Learn more about JIRA Service Desk development by trying one of our hands-on tutorials.
-   **<a href="https://answers.atlassian.com/questions/topics/31850522/jira-servicedesk-development" class="external-link">&quot;jira-servicedesk-development&quot; tag</a> on the Atlassian Answers forum** -- Join the discussion on JIRA Service Desk development.
-   **[Atlassian Connect documentation]**  -- Find all the information you need about Atlassian Connect, including release notes on the latest changes.

``` xml
<style>
iframe.youtube-player {
    height: 225px !important;
    margin: 20px;
}
a.aui-button.aui-button-primary {
    margin: 20px;
}
</style>
```

  [JIRA Service Desk REST API]: /cloud/jira/service-desk/jira-service-desk-cloud-rest-api
  [JIRA platform REST API]: /cloud/jira/platform/jira-cloud-platform-rest-api
  [JIRA Service Desk Webhooks]: /cloud/jira/service-desk/jira-service-desk-webhooks
  [JIRA Service Desk Modules]: /cloud/jira/service-desk/jira-service-desk-modules
  [introduction to Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/latest/guides/introduction.html
  [JIRA Cloud platform development]: /cloud/jira/platform/jira-cloud-development-platform
  [JIRA Service Desk tutorials]: /jiracloud/jira-service-desk-tutorials-39988298.html
  [Atlassian Connect documentation]: https://developer.atlassian.com/static/connect/docs/latest/index.html
