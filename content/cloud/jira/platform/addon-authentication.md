---
title: Authentication for add-ons
platform: cloud
product: jiracloud
category: devguide
subcategory: security
date: "2016-10-10"
---

{{< include path="content/cloud/connect/concepts/authentication.snippet.md" >}}