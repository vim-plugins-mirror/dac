---
title: Add-on descriptor JSON schemas
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/descriptor-schemas.html
- /jiracloud/descriptor-schemas.md
date: "2016-10-04"
---
{{< include path="content/cloud/connect/reference/descriptor-schemas.snippet.md">}}
