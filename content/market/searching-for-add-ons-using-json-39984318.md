---
title: Searching for Add Ons Using Json 39984318
aliases:
    - /market/searching-for-add-ons-using-json-39984318.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984318
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984318
confluence_id: 39984318
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Searching for add-ons using JSON

The `/rest/2/addons` API lets you query add-on listings. You may wish to use this, for instance, to write a script that outputs information about every add-on that meets some criteria.

For an example of such a script (using the Bash shell and `curl`), see `list-addons.sh` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Step 1: Getting the resource URL]
-   [Step 2: Optional query parameters]
-   [Step 3: Performing the query]
    -   [Request]
    -   [Response]
    -   [Response properties]

## Prerequisites

You will need:

-   Any [tool for making HTTP requests]

## Step 1: Getting the resource URL

The base URL for the add-on collection resource is currently `https://marketplace.atlassian.com/rest/2/addons` (with optional query parameters as described below).

Rather than hard-coding the URL path for the resource, you can also obtain it by starting from the entry point of the API. This takes additional steps, but is a better practice in case resource paths ever change in the future.

1.  Do a `GET` request to `https://marketplace.atlassian.com/rest/2` -- you will receive a JSON response that looks something like this:

    ``` js
    {
      "_links": {
        "self": {
          "href": "/rest/2"
        },
        [some more links...]
        "addons": {
          "href": "/rest/2/addons"
        }
      }
    }
    ```

2.  The `href` property under `addons` provides the URL path for the add-on collection resource.

 

## Step 2: Optional query parameters

The following optional parameters can be used to filter and/or sort your search query:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Parameter Name</th>
<th>Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><code>application</code></td>
<td>Only returns add-ons compatible with the specified application</td>
</tr>
<tr class="even">
<td><code>applicationBuild</code></td>
<td>Only returns add-ons compatible with the specified application build number; <code>application</code> parameter must also be specified</td>
</tr>
<tr class="odd">
<td><code>category</code></td>
<td><p>Only returns add-ons with the specified categories; <code>application</code> parameter must also be specified</p></td>
</tr>
<tr class="even">
<td><code>cost</code></td>
<td>Only returns add-ons with the specified paid/free status</td>
</tr>
<tr class="odd">
<td><code>filter</code></td>
<td>Return add-ons filtered or sorted using the specified parameter</td>
</tr>
<tr class="even">
<td><code>forThisUser</code></td>
<td>Only returns add-ons from vendors associated with the current user; you have to be authenticated to use this</td>
</tr>
<tr class="odd">
<td><code>hosting</code></td>
<td>Only returns add-ons with the specified hosting model</td>
</tr>
<tr class="even">
<td><code>includeHidden</code></td>
<td>Includes add-ons that are normally hidden in the site; default value is <code>false</code></td>
</tr>
<tr class="odd">
<td><code>includePrivate</code></td>
<td>Includes private add-ons if you are authorized to see them (you have to be authenticated to use this); default value is <code>false</code></td>
</tr>
<tr class="even">
<td><code>text</code></td>
<td>Only returns add-ons that match the search text</td>
</tr>
<tr class="odd">
<td><pre><code>withVersion</code></pre></td>
<td>Includes the latest compatible add-on version in the response (it will be in <code>/_embedded/versions</code> within each add-on item)</td>
</tr>
</tbody>
</table>

The `offset` and `limit` parameters are used for [pagination] to limit the number of add-ons returned by the search query.

Rather than referring to this table, you can get the list of supported parameters directly from the API:

-   Do a GET request to the add-on collection resource, adding `?limit=0` to indicate that you don't want any add-on data but only the links: `https://marketplace.atlassian.com/rest/2/addons?limit=0`
-   In the JSON response, look for the `query` link:

    ``` js
    {
      "_links": {
        [...other links that you can ignore...]
        "query": {
          "href": "/rest/2/addons{?application,applicationBuild,category*,cost,filter,forThisUser,hosting,includeHidden,includePrivate,text,withVersion,offset,limit}",
          "templated": true
        }
      }
    }
    ```

-   The `templated` property for this link indicates that it is a link template; in its `href` value, the symbols within curly braces indicate all the optional query parameters that you can use, using <a href="https://tools.ietf.org/html/rfc6570" class="external-link">URI Template</a> syntax.

## Step 3: Performing the query

### Request

-   HTTP method: `GET`
-   URL: the add-on collection resource, as described above, along with any optional query parameters
-   Authentication: if you want to see private or user-specific data (using `forThisUserOnly` or `includePrivate`), you must provide Basic Authentication with the username and password of your Atlassian Account

``` text
# get free add-ons that are compatible with JIRA
curl "https://marketplace.atlassian.com/rest/2/addons?application=jira&cost=free"
```

### Response

``` js
{
  "_links": {
    "self": {
      "href": "/rest/2/addons?application=jira&cost=free"
    },
    "alternate": {
      "href": "/plugins/app/jira?cost=free"
    }
    "next": [
      {
        "href": "/rest/2/addons?application=jira&cost=free&offset=10",
        "type": "application/json"
      },
      {
        "href": "/plugins/app/jira?cost=free&offset=10",
        "type": "text/html"
      }
    ]
  },
  "_embedded": {
    "addons": [
      {
        "_links": {
          "self": {
            "href": "/rest/2/addons/some-addon-key"
          }
        },
        "key": "some-addon-key",
        "name": "Some Addon",
        [... more add-on properties ...]
      },
      [... more add-on items ...]
    ]
  },
  "count": 125
}
```

### Response properties

-   `/_links/alternate`: A web link for viewing the corresponding list of results on the Marketplace site.

-   `/_links/next`: This will only be present if there are additional pages of results available after this one (the default size of a page is 10, unless you specify otherwise using `limit`). Note that is not a single link, but an array of two links: the first is a resource link for getting the next page via REST (content type "application/json"), and the second is a web link for displaying the next page of results on the Marketplace site (like the `alternate` link).

-   `/embedded/addons`: An array containing the add-on items. Note that these do not contain *all* properties of each add-on, but only a subset of the properties; to view all the properties, you can query the individual add-on resource using the `self` link for that item.

-   `/count`: the *total* number of results matched by the query, even if it is more than will fit in a single result page.

  [Prerequisites]: #prerequisites
  [Step 1: Getting the resource URL]: #step-1-getting-the-resource-url
  [Step 2: Optional query parameters]: #step-2-optional-query-parameters
  [Step 3: Performing the query]: #step-3-performing-the-query
  [Request]: #request
  [Response]: #response
  [Response properties]: #response-properties
  [tool for making HTTP requests]: /market/examples-of-api-usage-through-json-requests-39984261.html
  [pagination]: Marketplace-API_39368006.html#MarketplaceAPI-Pagination

