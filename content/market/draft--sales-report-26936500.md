---
title: Draft  Sales Report 26936500
aliases:
    - /market/draft--sales-report-26936500.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26936500
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26936500
confluence_id: 26936500
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : DRAFT - \_Sales Report

We provide you add-on sales information in two forms: 

-   The [Sales Report API] gives you access to the raw data on add-on sales via a REST API.
-   The sales and evaluation dashboard on the Atlassian Marketplace gives you a visual report and graph. The Sales and evals tab also uses the sales report API, so in either case, you are accessing the same information.  

### **Accessing your sales and evaluation report**

1.  Log into the Atlassian Marketplace with your vendor account.
2.  Click **Manage Add-ons** to visit your vendor profile page.
    <img src="/market/attachments/26936500/27230292.png" title="Click Manage add-ons" alt="Click Manage add-ons" class="confluence-thumbnail" width="300" />
3.  Click **Sales and evals** from the horizontal navigation bar.
    <img src="/market/attachments/26936500/27230294.png" title="Sales and evals tab" alt="Sales and evals tab" width="500" />

On the vendor dashboard page, the graph shows sales information for selected add-ons in a given time range. You can change the parameters to see information on specific add-ons or for a different time range.

The table below the graph shows information about recent sales. It lists several of the sales data fields shown in the table below, but not all. More complete information is available through the [Sales Report API]. What you see should look something like this: 

<img src="/market/attachments/26936500/27230295.png" title="Sales dashboard example" alt="Sales dashboard example" width="800" />

There are a few points to note about the data:

-   Evaluation data is not reported.
-   There is usually a 24-hour lag in reported sales data.

### **Data included in the sales report**

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Data</p></th>
<th><p>Description</p></th>
<th><p>Example</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Date</td>
<td>The date the sales transaction completed. The format for this field is <code>YYYY-MM-DD</code>.</td>
<td>2012-02-04</td>
</tr>
<tr class="even">
<td>Invoice</td>
<td>A unique transaction ID. Note that credit memos do not have the &quot;AT-&quot; prefix.</td>
<td>AT-123456</td>
</tr>
<tr class="odd">
<td><p>License ID</p></td>
<td><p>The unique identifier for the purchased license in Atlassian SEN format.</p></td>
<td><p>SEN-2345123</p></td>
</tr>
<tr class="even">
<td><p>Customer</p></td>
<td><p>The name of the purchasing organisation, and the name and email of the contact, if provided.</p></td>
<td><p>Example Co, Inc</p>
<p>Alice Adams</p></td>
</tr>
<tr class="odd">
<td><p>Product</p></td>
<td><p>The name of the purchased product.</p></td>
<td><p>MyZone for JIRA</p></td>
</tr>
<tr class="even">
<td><p>Sale Type</p></td>
<td><p>The type of sale from these options:</p>
<ul>
<li>New</li>
<li>Renewal</li>
<li>Upgrade</li>
</ul></td>
<td><p>New</p></td>
</tr>
<tr class="odd">
<td><p>License</p></td>
<td><p>The customer license type from these options:</p>
<ul>
<li>Commercial</li>
<li>Academic</li>
<li>Starter</li>
</ul></td>
<td><p>Commercial</p></td>
</tr>
<tr class="even">
<td><p>Users</p></td>
<td><p>The number of users permitted by the license and whether it is an enterprise license.</p></td>
<td><p>Enterprise +1000</p></td>
</tr>
<tr class="odd">
<td><p>Start</p></td>
<td><p>The start of the maintenance period associated with this sale. </p></td>
<td><p>2012-02-04</p></td>
</tr>
<tr class="even">
<td><p>End</p></td>
<td><p>The end of the maintenance period associated with this sale. The customer may renew before this date. </p></td>
<td><p>2013-02-04</p></td>
</tr>
<tr class="odd">
<td><p>Sale Price</p></td>
<td><p>The total sale amount before tax (GST) and including discounts. This value is negative for refunds.</p></td>
<td><p>100.00</p></td>
</tr>
<tr class="even">
<td>Atlassian</td>
<td>The amount from the sale due to Atlassian.</td>
<td>25.00</td>
</tr>
<tr class="odd">
<td>Vendor</td>
<td>The amount from the sale due to the vendor. This value is negative for refunds.</td>
<td>75.00</td>
</tr>
<tr class="even">
<td><p>Discount</p></td>
<td><p>Whether the sale is subject to any discounts, such as an Atlassian Expert discount. This value is negative for refunds.</p></td>
<td><p>25.00</p></td>
</tr>
</tbody>
</table>

  [Sales Report API]: https://developer.atlassian.com/display/UPM/Accessing+Sales+Reports+with+the+REST+API

