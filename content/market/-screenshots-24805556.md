---
title: Screenshots 24805556
aliases:
    - /market/-screenshots-24805556.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=24805556
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=24805556
confluence_id: 24805556
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Screenshots

1840 x 900px PNG/JPG for high-definition images, 920 x 450px PNG/JPG for standard images



