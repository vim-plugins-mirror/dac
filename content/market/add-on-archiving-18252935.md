---
title: Add On Archiving 18252935
aliases:
    - /market/add-on-archiving-18252935.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=18252935
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=18252935
confluence_id: 18252935
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Add-on archiving

This page explains why add-ons are archived. It also describes how add-on vendors can reinstate archived add-ons, so that they reappear as active listings on the Marketplace.

**On this page:**

-   [About add-on archiving]
-   [Accessing archived add-ons]
-   [Reinstating an archived add-on to active status]

## About add-on archiving

As the Marketplace grows, it's important for add-on users to be able to find current, relevant add-ons. For this reason we archive add-ons that are are no longer relevant or compatible with their Atlassian host product. This way Marketplace visitors can easily discover relevant add-ons, and reduces likelihood of finding outdated or obsolete add-ons.

The Marketplace archives add-ons automatically if the time since the end-of-life of the latest version of the Atlassian application it supports is greater than one year.

For EOL dates of Atlassian products and product versions, see <a href="https://confluence.atlassian.com/display/Support/Atlassian+Support+End+of+Life+Policy" class="external-link">Atlassian Support End of Life Policy</a>.

In some cases, an entire vendor may be archived, which means that all add-ons associated with that vendor are archived as well. 

## Accessing archived add-ons

Archived add-ons remain on the Marketplace, but they are mostly hidden. They are excluded from search results and from the various browsing views on the Marketplace.

Anyone can still access the listing, but only by going directly to the listing URL or by navigating to it from the archive link at the bottom of the website.

On the listing, a banner at the top of the page indicates that the add-on is archived.

Customers cannot buy or download the add-on for trial, but they can download the plugin and use it if they already have an existing license.

## Reinstating an archived add-on to active status

A month before an add-on is archived, the Atlassian Marketplace generates an automated email notifying the vendor.

The vendor can prevent archiving, or reinstate an add-on that is already archived, by updating the add-on listing to reflect support for an active version of the product.

It is up to the vendor to ensure that the add-on supports the product version. Once the vendor updates the listing, the listing automatically resumes active status, removing it from the archive list.

 

 

***
***

  [About add-on archiving]: #about-add-on-archiving
  [Accessing archived add-ons]: #accessing-archived-add-ons
  [Reinstating an archived add-on to active status]: #reinstating-an-archived-add-on-to-active-status

