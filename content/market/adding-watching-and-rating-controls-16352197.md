---
title: Adding Watching and Rating Controls 16352197
aliases:
    - /market/adding-watching-and-rating-controls-16352197.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=16352197
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=16352197
confluence_id: 16352197
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Adding watching and rating controls

Atlassian application administrators can install add-ons from the Marketplace website or through the <a href="https://confluence.atlassian.com/display/UPM/Universal+Plugin+Manager+Documentation" class="external-link">Universal Plugin Manager (UPM)</a>. Similarly, they can can watch or rate add-ons from either location. 

You can add features to your add-on to let users watch or rate your add-on inside your interface.

To create reviews and watch add-ons from a plugin, you use the `AtlassianMarketplaceUriFactory` interface in the UPM API. Behind the scenes, the calls use a REST API presented by UPM on the local Atlassian application instance. In your implementation, therefore, you are assembling URIs and posting them to the UPM API.

Note that the ability to review, rate, and watch add-ons through the UPM API is available in UPM 2.8 and later. The API checks that 2.8 is installed in the Atlassian application instance, along with ensuring other requirements are met. The same requirements apply as rating and watching add-ons from UPM, that is, the Atlassian application instance must have a valid SEN and the current user must be an administrator with a contact email domain associated with the SEN on MAC. 

## Creating a review

To create a review from your plugin code, follow these steps:

1.  Make sure your `pom.xml` defines the following dependency. If your add-on includes Atlassian UPM licensing, this dependency may already be present:

    ``` javascript
    <dependencies>
        ...
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>plugin-license-storage-lib</artifactId>
            <version>${upm.license.compatibility.version}</version>
        </dependency>
        ...
    </dependencies>
    ```

    The `upm.license.compatibility.version` should be set to 2.8 and later in the POM properties.

2.  Import the following package into your source file:

    ``` javascript
    import com.atlassian.upm.license.storage.lib.AtlassianMarketplaceUriFactory;
    ```

3.  Use the `AtlassianMarketplaceUriFactory` interface to construct a message based on the user input and post it, as in the following example:  

    ``` java
    ...
    if (uriFactory.canReviewPlugin())
    {
        // make sure review form is only shown when uriFactory.canReviewPlugin() returns true
        ...
        URI reviewUri = uriFactory.getReviewPluginUri(4 /*stars*/, some("This is my review message"));
        PostMethod post = new PostMethod(reviewUri);
        post.setRequestHeader("Content-Type","application/vnd.atl.plugins.available.plugin+json")
        httpClient.execute(post);
        ...
    }
    ...
    ```

    In this case, we've hard-coded the rating value and review text. In your own plugin, replace these values with user-provided values.

## Watching and unwatching an add-on

Add-ons can similarly expose UI controls that enable users to watch your add-on. A user who watches an add-on receives email notifications when an update to the add-on is available.

Implementing add-on watch controls is similar to setting up add-on ratings. It uses the same interface, `AtlassianMarketplaceUriFactory`. If you haven't already, follow the steps in [Creating a review from an add-on] to add the required POM dependency and import the appropriate package.

The following sample shows the code for watching and stop watching the add-on:

``` java
...
if (uriFactory.canWatchPlugin())
{
    // make sure watch link is only shown when uriFactory.canWatchPlugin() returns true
    ...
    // to watch a plugin
    URI watchUri = uriFactory.getWatchPluginUri();
    PostMethod post = new PostMethod(watchUri);
    post.setRequestHeader("Content-Type","application/vnd.atl.plugins.available.plugin+json")
    httpClient.execute(post);
    ...
 
    // to unwatch a plugin
    URI unwatchUri = uriFactory.getWatchPluginUri();
    DeleteMethod delete = new DeleteMethod(unwatchUri);
    httpClient.execute(delete);
}
...
```

  [Creating a review from an add-on]: #creating-a-review-from-an-add-on

