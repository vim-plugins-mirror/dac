---
title: Licensing and Paid via Atlassian Listings 13632223
aliases:
    - /market/licensing-and-paid-via-atlassian-listings-13632223.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=13632223
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=13632223
confluence_id: 13632223
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Licensing and Paid-via-Atlassian Listings

To list a Paid-via-Atlassian plugin, you must use Atlassian's licensing system. You can do so by using the Atlassian Licensing Support API in your plugin. You can find a tutorial here [Tutorial: Adding licensing support to your add-on (up to UPM version 2.0)].

Just like Atlassian products, customers can get an evaluation license or purchase a standard Atlassian license for a Paid-via-Atlassian listing. Customers receive an evaluation license when they choose the Free 30 day Trial button on the Marketplace or the Try button in the Universal Plugin Manager (UPM). An evaluation license allows a customer to use an add-on until the evaluation period end-date is reached. After the 30-day evaluation period, the add-on's license enforcement mechanism engages and the add-on becomes invalid. (As the add-on vendor, you determine how the add-on functions in response to an invalid evaluation license).

Standard Atlassian licenses allow users to perpetually use an add-on. Customers receive a standard license when they choose the Buy License Now from the Marketplace or the Buy button in the UPM. A standard license gives your customer maintenance for a year from date of purchase. Maintenance includes support and access to any version upgrades for a year from date of purchase. One year after purchase, the maintenance expires and the customer must renew the license to receive support or maintenance for the next 12 month period.

Maintenance is valid for one year after purchase. The maintenance-start and -end date are encoded into the license. The Atlassian Licensing system reports whether licenses are in maintenance. To determine license maintenance for a given version of your add-on, the maintenance-start and -end date of the license is compared against the build-date of your add-on.

Find out more about maintenance here: <a href="http://www.atlassian.com/licensing/purchase-licensing#softwaremaintenance" class="uri" class="external-link">http://www.atlassian.com/licensing/purchase-licensing#softwaremaintenance</a>

Just like Atlassian products, customers can get an evaluation license or purchase a standard Atlassian license for a Paid-via-Atlassian listing. Customers receive an evaluation license when they choose the Free 30 day Trial button on the Marketplace or the Try button in the Universal Plugin Manager (UPM). An evaluation license allows a customer to use an add-on until the evaluation period end-date is reached. After the 30-day evaluation period, the add-on's license enforcement mechanism engages and the add-on becomes invalid. (As the add-on vendor, you determine how the add-on functions in response to an invalid evaluation license).

Find out more: <a href="http://www.atlassian.com/licensing/purchase-licensing#evaluations" class="uri" class="external-link">http://www.atlassian.com/licensing/purchase-licensing#evaluations</a>

To support education and encourage the next generation of software developers, Atlassian offers all of our software at a 50% discount to qualified academic institutions. Paid-via-Atlassian add-ons are also available to qualified institutions at the same reduced price. See more here:

<a href="http://www.atlassian.com/licensing/purchase-licensing#pricinganddiscounts-5" class="uri" class="external-link">http://www.atlassian.com/licensing/purchase-licensing#pricinganddiscounts-5</a>

Atlassian supports organizations that seek to do good in the world. Charitable organizations can apply for no-cost Community licenses. Once a customer has a Community license for any Atlassian product, they can request additional licenses from Atlassian sales representatives, including Community licenses for add-ons they obtain through the Marketplace.

Your customers can apply for an Atlassian Community license here:

<a href="http://www.atlassian.com/software/views/community-license-request" class="uri" class="external-link">http://www.atlassian.com/software/views/community-license-request</a>

Customers with an existing Community license for any Atlassian product, should request a Community license for your add-on by emailing <a href="mailto:sales@atlassian.com" class="external-link">sales@atlassian.com</a>.

Atlassian wants to support the Open Source community that has helped Atlassian come so far, open source projects can <a href="http://www.atlassian.com/software/views/open-source-license-request" class="external-link">apply for no-cost Open Source licenses</a>. Approved organizations can request Open Source licenses for Paid-via-Atlassian add-ons by contacting Atlassian sales representatives. These representative validate and issue Community licenses.

<a href="http://www.atlassian.com/software/views/community-license-request" class="uri" class="external-link">http://www.atlassian.com/software/views/community-license-request</a>

Marketplace add-on customers can get developer licenses for add-ons just as they can for host Atlassian products. Developer licenses are intended for staging or development environments only. Customers who have purchased Marketplace add-ons can get developer licenses by clicking the **View Developer License** link from their license page on <a href="http://my.atlassian.com/" class="external-link">My.Atlassian.com</a>.

The license types between the add-on and host application must match. That is, an add-on with a developer license only works in a host application that also has a developer license. The same applies to production licenses--a host application with a production license only takes add-ons that also have production licenses. If an add-on license type doesn't match its host application, the Universal Plugin Manager reports a license error status of "Incompatible with product license (wrong type)" for that add-on.

Find out how to create developer licenses here:

<a href="http://www.atlassian.com/licensing/purchase-licensing#licensing-7" class="uri" class="external-link">http://www.atlassian.com/licensing/purchase-licensing#licensing-7</a>

At this time, there is no automatic way to issue yourself a license to your Marketplace add-on. We may add this in the future. In the meantime you can use Marketplace Promotions to create any 100% discount licenses you need for internal purposes. Promotions are found on your "Manage Vendor" page on <a href="https://Marketplace.Atlassian.com" class="external-link">Marketplace.Atlassian.com</a>.

The Atlassian Marketplace doesn't offer multi-node licenses for Paid-via-Atlassian add-ons.

Confluence customers with multiple, clustered Confluence nodes need to use different add-on licenses for each node. See more information on multiple licenses at the Marketplace Licensing FAQ: <a href="http://www.atlassian.com/licensing/marketplace#licensingandpricing-4" class="uri" class="external-link">http://www.atlassian.com/licensing/marketplace#licensingandpricing-4</a>.

Marketplace customers can purchase maintenance for one, two, or three years.

Yes. Find out more here: <a href="http://www.atlassian.com/licensing/purchase-licensing#softwaremaintenance-5" class="uri" class="external-link">http://www.atlassian.com/licensing/purchase-licensing#softwaremaintenance-5</a>

To transition smoothly into the Marketplace, vendors can, at no charge, convert an existing non-Atlassian license into a valid Atlassian Marketplace license. These licenses are generated for each customer and assigned to that customer's MyAtlassian account. Customers who do not have those accounts will have accounts generated for them automatically and the licenses attached.

You can convert an old license into a license compatible with the Atlassian licensing system using the Marketplace REST API. For more information, see [Converting proprietary licenses into Marketplace licenses].

 

The licensing solution provided by the Universal Plugin Manager (UPM) supports the following products:

-   Confluence 3.1 (released in December 2009) and newer
-   JIRA 4.1 (released in April 2010) and newer
-   Bamboo 2.6 (released in June 2010) and newer
-   FishEye/Crucible 2.4+ (released in October 2010) and newer

There are two licensing techniques you can use in your add-on code. One technique works for UPM 2.1 and up. The other technique works for earlier versions of UPM. Which licensing technique you implement depends on the versions of Atlassian products your customers are likely to use with your add-on.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Licensing Technique</p></th>
<th><p>When to use this technique?</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Plugin License Storage Library + UPM</p></td>
<td><p>You have an existing customer base using a mix of Atlassian product versions. <em>This is the situation for most vendors</em>. You should use the techniques described here <a href="/market/8947417.html">Tutorial: Adding licensing support to your add-on (up to UPM version 2.0)</a>.</p></td>
</tr>
<tr class="even">
<td><p>UPM Licensing API</p></td>
<td><p>You have a brand new plugin. That plugin only works on the latest versions of Atlassian products and all of those products bundle UPM 2.1 or later. You should use the techniques described here <a href="/pages/createpage.action?spaceKey=UPM&amp;title=How+to+Add+Licensing+Support+to+a+Marketplace-Ready+Plugin" class="createlink">How to Add Licensing Support to a Marketplace-Ready Plugin</a>.</p></td>
</tr>
</tbody>
</table>

Yes, you can. Use the `PluginLicense.getSupportEntitlementNumber()` method to get this value.

  [Tutorial: Adding licensing support to your add-on (up to UPM version 2.0)]: /market/8947417.html
  [Converting proprietary licenses into Marketplace licenses]: /market/converting-proprietary-licenses-into-marketplace-licenses-10421889.html

