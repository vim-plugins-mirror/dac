---
title: Sizes Marketing 11305282
aliases:
    - /market/-sizes-marketing-11305282.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305282
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305282
confluence_id: 11305282
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Sizes Marketing

Each child page contains a value that appears on multiple pages in the MARKET and the UPM. Change the excerpt text on the appropriate child page and you change it everywhere it appears in both spaces. To include a page in another use the `excerpt-include` macro:

    {excerpt-include:PAGENAME|nopanel=true}

The following pages are used for setting these values:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Global Variable Page</p></th>
<th><p>Contains this value</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p><a href="/market/-addon-icon-11305305.html">_Addon Icon</a></p></td>
<td>16x16px PNG/JPG/GIF</td>
</tr>
<tr class="even">
<td><p><a href="/market/-addon-logo-11305301.html">_Addon Logo</a></p></td>
<td>144 x 144px PNG/JPG</td>
</tr>
<tr class="odd">
<td><p><a href="/market/-banner-11305309.html">_Banner</a></p></td>
<td>1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</td>
</tr>
<tr class="even">
<td><p><a href="/market/-carousel-11305317.html">_Carousel</a></p></td>
<td>560x274px PNG/JPG</td>
</tr>
<tr class="odd">
<td><p><a href="/market/-screenshot-11305313.html">_Screenshot</a></p></td>
<td>Displayed at 560px x 274px in carousel, no maximum size for lightbox. PNG/JPG</td>
</tr>
<tr class="even">
<td><p><a href="/market/-vendor-icon-11305293.html">_Vendor Icon</a></p></td>
<td>16x16px PNG/JPG/GIF</td>
</tr>
<tr class="odd">
<td><p><a href="/market/-vendor-logo-11305287.html">_Vendor Logo</a></p></td>
<td>72x72px PNG/JPG/GIF</td>
</tr>
<tr class="even">
<td><a href="/market/-highlight-screenshot-23298390.html">_Highlight Screenshot</a></td>
<td>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</td>
</tr>
<tr class="odd">
<td><a href="/market/-highlight-screenshot-cropped-24805553.html">_Highlight Screenshot Cropped</a></td>
<td>580 x 330px PNG/JPG</td>
</tr>
<tr class="even">
<td><a href="/market/-screenshots-24805556.html">_Screenshots</a></td>
<td><p>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</p></td>
</tr>
</tbody>
</table>

## Chained Variables

The \_Sizes Marketing include in the MARKET space contains global variables. These variables are also included via what I am calling chained includes in the UPM space. Setting the variables in the MARKET space pages sets them in the UPM space as well.

This construction is necessary because the `excerpt` macro does not allow for a `SPACE` qualifier that you see in an `include` macro.

### How it works

The MARKET space contains these pages:

-   \_Sizes Marketing
-   \_Addon Icon
-   \_Addon Logo
-   \_Banner
-   \_Carousel
-   \_Screenshot
-   \_Vendor Icon
-   \_Vendor Logo

The UPM space contains the identically named pages. You can use the same `excerpt` calls in the UPM space:

    {excerpt-include:PAGENAME|nopanel=true}

The difference is the child page in UPM, for example, `_Add Logo`, contains a reference to the MARKET space:

    {excerpt}{include:MARKET:_Addon Icon}{excerpt}

In this way, changing the value in the `MARKET:_Addon Icon` file ensures both spaces use the same global value.

  [\_Addon Icon]: /market/-addon-icon-11305305.html
  [\_Addon Logo]: /market/-addon-logo-11305301.html
  [\_Banner]: /market/-banner-11305309.html
  [\_Carousel]: /market/-carousel-11305317.html
  [\_Screenshot]: /market/-screenshot-11305313.html
  [\_Vendor Icon]: /market/-vendor-icon-11305293.html
  [\_Vendor Logo]: /market/-vendor-logo-11305287.html
  [\_Highlight Screenshot]: /market/-highlight-screenshot-23298390.html
  [\_Highlight Screenshot Cropped]: /market/-highlight-screenshot-cropped-24805553.html
  [\_Screenshots]: /market/-screenshots-24805556.html

