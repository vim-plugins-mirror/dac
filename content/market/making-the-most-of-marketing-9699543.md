---
title: Making the Most Of Marketing 9699543
aliases:
    - /market/making-the-most-of-marketing-9699543.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=9699543
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=9699543
confluence_id: 9699543
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Making the most of marketing

Your listing on Atlassian Marketplace is just the first opportunity you have to market your add-on. You can do a lot more to promote your add-on and leverage the Atlassian channels to reach more potential customers. This page addresses best practices and suggestions for marketing your add-on in and outside of the Atlassian Marketplace.

### First things - "<a href="http://event.on24.com/wcc/r/1243158/ACA99BDAA00F9AEC5E0F8D93E38F2277" class="external-link">How to Market Your Add-on Like a Pro</a>" 

Watch this one hour <a href="http://event.on24.com/wcc/r/1243158/ACA99BDAA00F9AEC5E0F8D93E38F2277" class="external-link">recorded webinar</a> that covers many basic tactics and metrics in your first GTM plan across three key areas:

-   Optimize Your Listing
-   Acquire Users
-   Retain Users

 

------------------------------------------------------------------------

 

-   [Market your add-on in the Atlassian Marketplace]
-   [Promoting your add-on]
-   [Optimize search whenever you can]
-   [Getting help from Atlassian marketing]
-   [Don't stop there!]

## Market your add-on in the Atlassian Marketplace

The Marketplace [requires branding materials] to list your add-on on the site, whether your add-on is free or paid. In usability testing, customers express preferences for multiple small screenshots, shorter text elements, and easy-to-digest highlights. For this reason, we require three highlights for every add-on in the Marketplace. This also provides a uniform visual experience from add-on to add-on, and lets Marketplace visitors quickly learn what three things your add-on can do for them.

If you're struggling to come up with three distinct highlights for your add-on, we suggest framing your add-on in different contexts. For example, how does your add-on make working in an Atlassian application easier? What are the broad goals people can meet using your add-on? Does it save time? Does it improve admin tasks? 

Oppositely, if you are unsure how to trim your add-on benefits down to only three, consider a similar approach. Instead of listing all the things your add-on can do, list the most salient benefits. We suggest is choosing three distinct high-level benefits. 

## Promoting your add-on

You want your add-on to stand out. There are several opportunities for you to promote your add-on. It is important to have consistent, tightly-written, and kick-ass promotional materials.

-   You can create a landing page on your own company website. On a landing page, you can go into greater level of detail than your listing. If your add-on is not paid-via-Atlassian, your landing page is the place where your customers can buy your add-on. Your landing page should link back to Atlassian Marketplace. 
-   The Atlassian Verified badge can be used to promote your add-on on your company website. Please note, this is for [Verified vendor] use only.

<img src="/market/attachments/9699543/29524431.png" width="400" />

-   Blogs and other social media are great ways to promote your product. For example, you can use Twitter to send out quick reminders about your add-on or your company. (A hash tag that reads \#atlassian or \#*atlassian\_product* reaches even more readers.) Remember your existing customers already read your blog and they already use your product. Your blog should emphasize use cases, quick tips for getting more out of the add-on, and of course upcoming releases.

## Optimize search whenever you can

When creating and marketing online content, think about search engine optimization (SEO). SEO affects the discoverability/ranking of your content. You should optimize your Marketplace listing and all your promotional material for SEO. Check out the <a href="http://www.google.com/support/webmasters/bin/answer.py?answer=35291" class="external-link">Google webmaster tools help</a> and the <a href="https://adwords.google.com/select/KeywordToolExternal" class="external-link">Google keyword tool</a>.

## Getting help from Atlassian marketing

Atlassian promotes certain add-ons independently through our <a href="http://blogs.atlassian.com/" class="external-link">product blogs</a>, guest blogs, email newsletters, social media mentions, product webinars, and featured listings, both on our company website and on the Atlassian Marketplace.

In general, Atlassian prioritizes marketing support in favor of Paid-via-Atlassian add-ons and focuses on general promotion of the Atlassian Marketplace, rather than individual comarketing with vendors. However, we're always on the lookout for new opportunities. Just <a href="mailto:marketplace@atlassian.com" class="external-link">drop us a line</a> and we will triage your request and get back to you.

 

## Don't stop there!

Don't just launch your add-on and then forget about it.

-   Integrate your Atlassian Marketplace listing with Google Analytics.
-   Read your <a href="https://plugins.atlassian.com/review/plugin/256" class="external-link">User reviews</a> and respond to customer feedback.

The more data you track and record (be conscious of <a href="https://www.atlassian.com/legal/privacy-policy" class="external-link">Atlassian's privacy policy</a> and your own privacy policy, along with the need to anonymize), the better you are able to fine-tune your add-on to be successful.

  [Market your add-on in the Atlassian Marketplace]: #market-your-add-on-in-the-atlassian-marketplace
  [Promoting your add-on]: #promoting-your-add-on
  [Optimize search whenever you can]: #optimize-search-whenever-you-can
  [Getting help from Atlassian marketing]: #getting-help-from-atlassian-marketing
  [Don't stop there!]: #dont-stop-there!
  [requires branding materials]: /market/branding-your-add-on-9699545.html
  [Verified vendor]: /market/atlassian-verified-program-23299224.html

