---
title: Licensing API Overview 8947414
aliases:
    - /market/licensing-api-overview-8947414.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=8947414
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=8947414
confluence_id: 8947414
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Licensing API Overview

 

Starting with the Atlassian SDK 3.7, you can add the license management module to your plugin projects. This module gives plugins access to the license enforcement features in the UPM across products.

-   [Contents of the License Management Module]
-   [The LicenseChecker Class]
-   [Custom License Operations]

## Contents of the License Management Module

You can use any of the `atlas-create-`*APPLICATION-*`plugin` commands to create a project ( for example, `atlas-create-jira-plugin`). Once you have a plugin project, you use the SDK's `atlas-create-`*APPLICATION-*`plugin-module` to generate the Atlassian License Management module.

The module generator adds a `LicenseChecker` class to your project. This class listens for Atlassian licensing events and disables the plugin if its license is invalid. The module modifies your plugin's descriptor file (`atlassian-plugin.xml`) by:

-   enabling the `atlassian-licensing-enabled` parameter to `true` in `<plugin-info>`.
-   importing the `com.atlassian.plugin.PluginController` component
-   importing the `com.atlassian.upm.api.license.PluginLicenseEventRegistry` component
-   importing the `com.atlassian.upm.api.license.PluginLicenseEventRegistry` component

Finally, the generator adds the Maven dependencies on UPM 2.0 to the project `pom.xml` file.

## The `LicenseChecker` Class

The `LicenseChecker` class uses the UPM's licensing API to listen to plugin license events and enforce license validity. The class constructor takes a `PluginController`, a `PluginLicenseManager`, and a `PluginEventRegistry`. In addition to a constructor, this generated class contains the following methods:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><code>handleEvent</code></p></td>
<td><p>An overloaded method that listens for:</p>
<ul>
<li>Enable events - either at application startup time, or when the plugin is installed into an already-running application. The plugin may or may not have a license at this point.</li>
<li>Changes to the plugin license other than its complete removal.</li>
<li>Removal of an installed plugin.</li>
</ul></td>
</tr>
<tr class="even">
<td><p><code>checkLicense()</code></p></td>
<td><p>Checks for invalid licenses and disables the plugin if its license is invalid.</p></td>
</tr>
<tr class="odd">
<td><p><code>isValidLicense()</code></p></td>
<td><p>Determines if a license is valid. Logs appropriate messages.</p></td>
</tr>
</tbody>
</table>

Atlassian designed the class to handle basic license operations. After you add the module, no additional coding is required.

## Custom License Operations

You can customize the licensing behavior of your plugin using the `PluginLicenseManager` and the `PluginLicenseEventRegistry` interfaces. Both the `PluginLicenseManager` and the `PluginLicenseEventRegistry` instances are specific to your plugin.

### `PluginLicenseManager`

To do this, you can override the plugin-disablement and implement your own enforcement behavior through the `PluginLicenseManager` class. Each plugin using UPM's Licensing API is given a `PluginLicenseManager` instance specific to their plugin. This class has two methods:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p><code>getLicense()</code></p></td>
<td><p>Fetches the current plugin's license.</p></td>
</tr>
<tr class="even">
<td><p><code>getPluginKey()</code></p></td>
<td><p>Fetches the current plugin's key.</p></td>
</tr>
</tbody>
</table>

Using these methods, you can check the license status, as in the following form. If appropriate, the code can disable the plugin by its key.

``` java
if (licenseManager.getLicense().isDefined())
{
   PluginLicense license = licenseManager.getLicense().get();
   if (license.getError().isDefined())
   {
        // handle license error scenario
        // (e.g., expiration or user mismatch) 
   }
   else
   {
        // handle valid license scenario
   }
} 
else 
{
        // handle unlicensed scenario
}
```

We recommend that the plugin checks license validity at each of the plugin's entry points. Upon detecting an invalid license you can either hide all UI elements, make your plugin read-only, disable your plugin, or any other behavior your define.

### `PluginLicenseEventRegistry`

If you want your plugin to, for example, handle a license submission or expiration in a specific manner you can use `PluginLicenseEventRegistry` to listen for a variety of license events. The `com.atlassian.upm.api.license.event` package supports the following event types:

-   `PluginLicenseAddedEvent`
-   `PluginLicenseChangeEvent`
-   `PluginLicenseCheckEvent`
-   `PluginLicenseExpiredEvent`
-   `PluginLicenseMaintenancePeriodExpiredEvent`
-   `PluginLicenseRemovedEvent`
-   `PluginLicenseUpdatedEvent`

##### RELATED TOPICS

-   [Tutorial: Adding licensing support to your add-on (up to UPM version 2.0)] 
-   [Referencing visual assets in your add-on JAR]
-   [JavaDoc]

 

  [Contents of the License Management Module]: #contents-of-the-license-management-module
  [The LicenseChecker Class]: #the-licensechecker-class
  [Custom License Operations]: #custom-license-operations
  [Tutorial: Adding licensing support to your add-on (up to UPM version 2.0)]: /market/8947417.html
  [Referencing visual assets in your add-on JAR]: /market/referencing-visual-assets-in-your-add-on-jar-9699404.html
  [JavaDoc]: https://developer.atlassian.com/static/

