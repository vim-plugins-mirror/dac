---
title: 26936649
aliases:
    - /market/26936649.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=26936649
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=26936649
confluence_id: 26936649
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Automatic Atlassian Connect add-on polling & versioning

We automatically detect updates to Atlassian Connect add-ons with a polling service. We poll the add-on descriptor URL that you included when you submitted your listing every minute. When we detect a change, we automatically update your add-on in the Atlassian Marketplace with a new version. The way we increment your version number depends on the changes made to your descriptor. 

We poll and update your add-on so that you can easily release fixes and new features, without having to manually create new version entries on Marketplace. We want to ensure that customers get the latest version of your add-on with as little delay as possible - Connect add-ons should seem like web services, not versioned software. 

## Major, minor, and micro version update definitions

Updates are published to the Marketplace within a few minutes of detecting changes from your [Atlassian Connect descriptor]. Customers automatically receive updates via the UPM within 6 hours. Any change to your service that *does not alter your add-on descriptor* will be immediately available to users and will not result in a new version on Marketplace.

We automatically build a version identifier for your add-on when we detect the following changes:

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Level incremented</th>
<th>Changes detected</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Major version</p>
<p>1.2.3 to 2.0.0</p></td>
<td><p><a href="https://developer.atlassian.com/static/connect/docs/modules/#apiVersion">API version update</a> (for example, 1.0 to 2.0)</p>
<p><img src="/market/images/icons/emoticons/information.png" alt="(info)" /> The major version matches the API version listed in your descriptor.</p></td>
</tr>
<tr class="even">
<td><p>Minor version</p>
<p>1.2.3 to 1.3.0</p></td>
<td><p>Increase or changes in scope, <strong>and/or</strong> Transition from free to paid model</p>
<p><img src="/market/images/icons/emoticons/information.png" alt="(info)" /> Customers must manually approve updates for minor version updates</p></td>
</tr>
<tr class="odd">
<td><p>Micro version</p>
<p>1.2.3 to 1.2.4</p></td>
<td><p>Any descriptor changes not included above that do <strong>not</strong> require manual approval</p></td>
</tr>
</tbody>
</table>

Changes that require manual customer approval

Even though your add-on is automatically updated in the Marketplace, certain scenarios require customers to manually approve your add-on's update in the <a href="https://confluence.atlassian.com/x/_AJTE" class="external-link">UPM</a>. These changes correspond to minor version updates in the table above. In these cases, we automatically send emails to the product administrator so they can approve and update the add-on.

These scenarios require manual customer approval:

-   Your listing changes from **free to paid**: Your change triggers a Marketplace approval. Existing customers need to approve the change to start paying for your add-on, otherwise they will need to uninstall it.
-   Your listing involves **additional [scopes]**: Marketplace updates happen automatically (no approval necessary), but customers need to approve the changes to continue using your add-on.

Before the approval, those customers continue to use the older version of your descriptor. If you can plan ahead, it's a good idea to isolate those changes from any other changes in functionality.

## Viewing automatically added versions

You can view add-ons in the Marketplace the same way you manage other add-on versions: 

1.  Log in with your vendor credentials.
2.  Click **Manage listings **from the header.
3.  Click your add-on's name from the list.
4.  Click **Versions** in the horizontal navigation bar.
5.  You'll see updates from *Marketplace Hub \[Atlassian\]:*
    *<img src="/market/attachments/26936649/27230318.png" width="700" />
    *

 

 

 

  [Atlassian Connect descriptor]: https://developer.atlassian.com/static/connect/docs/modules/
  [API version update]: https://developer.atlassian.com/static/connect/docs/modules/#apiVersion
  [(info)]: /market/images/icons/emoticons/information.png
  [scopes]: https://developer.atlassian.com/static/connect/docs/scopes/scopes.html

