---
title: Revenue Share Example 10420459
aliases:
    - /market/-revenue-share-example-10420459.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420459
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420459
confluence_id: 10420459
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Revenue share example

Consider an add-on that is licensed for $100.00, a standard sale and a sale under the Experts discount program look like this:

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p> </p></th>
<th><p>Standard sale</p></th>
<th><p>Experts sale</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>License cost</p></td>
<td><p>100.00</p></td>
<td><p>80.00</p></td>
</tr>
<tr class="even">
<td><p>Your revenue</p></td>
<td><p>75.00</p></td>
<td><p>60.00</p></td>
</tr>
<tr class="odd">
<td><p>Atlassian revenue</p></td>
<td><p>25.00</p></td>
<td><p>20.00</p></td>
</tr>
</tbody>
</table>



