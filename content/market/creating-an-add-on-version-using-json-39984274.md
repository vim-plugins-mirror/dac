---
title: Creating an Add On Version Using Json 39984274
aliases:
    - /market/creating-an-add-on-version-using-json-39984274.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984274
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984274
confluence_id: 39984274
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Creating an add-on version using JSON

The `/rest/2/addons` API includes a REST resource for creating add-on versions. You might want to do this as part of an automated release process. The manual equivalent would be to go to <a href="https://marketplace.atlassian.com/manage/plugins" class="external-link">Manage listings</a> on Marketplace, select one of your existing add-ons, select "Create version" and fill in the version creation form.

For an example of such a script (using the Bash shell and `curl`), see `create-addon-version.sh` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Step 1: Upload the installable artifact, if any]
    -   [Request]
    -   [Response]
-   [Step 2: Build a JSON document for the new version]
    -   [Descriptions of version properties]
-   [Step 3: Post the new version to Marketplace]
    -   [Request][1]
    -   [Response][2]
-   [Approval for new versions]

## Prerequisites

You will need:

-   Any [tool for making HTTP requests][]<a href="https://curl.haxx.se/" class="external-link"></a>
-   An existing add-on listing on Marketplace (can be public or private)
-   The credentials of your Atlassian account
-   If the add-on is directly installable, the installable artifact (`.jar`, `.obr` or workflow file) for the new add-on version

## Step 1: Upload the installable artifact, if any

If the add-on is directly installable, you will need to upload the artifact (`.jar`, `.obr` or workflow file) to the Marketplace server.

You will do this by sending the file data in an HTTP POST to the resource that the Marketplace API provides for this purpose; the URL path for this resource is currently `/rest/2/assets/artifact`.

Rather than hard-coding the URL path for the resource, you can also obtain it by starting from the entry point of the API. This takes additional steps, but is a better practice in case resource paths ever change in the future.

1.  Do a `GET` request to `https://marketplace.atlassian.com/rest/2` -- you will receive a JSON response that looks something like this:

    ``` js
    {
      "_links": {
        "self": {
          "href": "/rest/2"
        },
        [some more links...]
        "assets": {
          "href": "/rest/2/assets"
        }
      }
    }
    ```

2.  The `href` property under `assets` is the URL path for the general area of the API that deals with uploading files. Do a `GET` request to this resource (`https://marketplace.atlassian.com/rest/2/assets`) -- you will receive a JSON response like this:

    ``` js
    {
      "_links": {
        "self": {
          "href": "/rest/2/assets"
        },
        "artifact": {
          "href": "/rest/2/assets/artifact"
        }
    }
    ```

3.  Now, the `href` property under `artifacts` gives you the URL path for the resource you'll use to upload your artifact.

#### Request

-   HTTP method: `POST`
-   URL: `https://marketplace.atlassian.com/rest/2/assets/artifact?file=LOGICAL_FILE_NAME`
    -   For `LOGICAL_FILE_NAME`, substitute whatever you want the file to be called when a user downloads it, for instance `my-addon-1.0.jar`
-   Authentication: Basic Authentication, with the username and password of your Atlassian Account
-   Content type: can be `application/octet-string` or leave it unspecified
-   Request body: the entire contents of the file

``` text
curl --basic --user MY_USERNAME \
   -H "Content-Type: application/octet-stream" \
   --data-binary @/LOCATION_OF_MY_FILE/my-addon-1.0.jar \
   https://marketplace.atlassian.com/rest/2/assets/artifact?file=my-addon-1.0.jar
```

#### Response

``` js
{
  "_links": {
    "self": {
      "href": "/rest/2/assets/artifact%2F1324354154%2Fmy-addon-1.0.jar"
    }
  } [...you can ignore the rest of the JSON properties...]
}
```

Copy the value of the `self.href` property (in this case, `"/rest/2/assets/artifact%2F1324354154%2Fmy-addon-1.0.jar"`); you will need it in the next step.

If Marketplace does not recognize the file as a valid installable artifact, the request will return an HTTP 400 error and a description of the problem.

## Step 2: Build a JSON document for the new version

Here is an example of a JSON document with some of the properties you will normally want to specify.

``` js
{
  "_links": {
    "artifact": {
      "href": "/rest/2/assets/artifact%2F1324354154%2Fmy-addon-1.0.jar"
    }
  },
  "name": "1.0",
  "buildNumber": 100,
  "status": "private",
  "paymentModel": "free",
  "compatibilities": [
    {
      "application": "jira",
      "server": {
        "min": {
          "build": 70107
        },
        "max": {
          "build": 71003
        }
      }
    }
  ],
  "release": {
    "date": "2016-03-17",
    "beta": false,
    "supported": false
  },
  "text": {
    "releaseSummary": "here is a new version"
  },
  "vendorLinks": {
  }
}
```

#### Descriptions of version properties

-   `/_links/artifact`:
    -   If you uploaded an artifact as shown in step 1, the `href` property here should be set to the same value that you received in the upload response.
    -   Or, if you did not upload an artifact, but it exists on a publicly accessible server (such as a Maven repository), you can simply set `_links.artifact.href` to the URL of the artifact (for instance, `"http://my-server.example.com/url/to/download/my-addon-1.0.jar"`) so Marketplace knows where to find it.
    -   Or, if this is not a directly installable add-on, you can omit `_links.artifact` entirely.
-   `/name`: the version string. You can omit this if it is a directly installable add-on with a `.jar` or `.obr` file, since Marketplace can read the version string from the file.
-   `/buildNumber`: build numbers are unique values you choose to determine version ordering.
-   `/status`: A version can be `private` or `public`.
-   `/paymentModel`: A version can be free (`"free"`), paid via vendor (`"vendor"`), or paid via Atlassian (`"atlassian"`).
-   `/compatibilities`: You can entirely omit this property if the new add-on version has the same Atlassian application compatibility as the previous add-on version. Otherwise, you will need to specify the application key and (for Server add-ons) the minimum and maximum application build numbers.
-   `/release/date`: This date will appear in the add-on's version history.
-   `/release/beta`: Set to `true` if this is a beta version.
-   `/release/supported`: Set to `true` if this version is officially supported.
-   `/text/releaseSummary`: A brief description of the release.

## Step 3: Post the new version to Marketplace

#### Request

-   HTTP method: `POST`
-   URL: `https://marketplace.atlassian.com/rest/2/addons/ADDON_KEY/versions`

    Rather than hard-coding the URL path for the resource, you can also obtain it by starting from the entry point of the API. This takes additional steps, but is a better practice in case resource paths ever change in the future.

    1.  Do a `GET` request to `https://marketplace.atlassian.com/rest/2` -- you will receive a JSON response that looks something like this:

        ``` js
        {
          "_links": {
            "self": {
              "href": "/rest/2"
            },
            [some more links...]
            "addons": {
              "href": "/rest/2/addons"
            }
          }
        }
        ```

    2.  The `href` property under `addons` is the URL path of the resource for finding add-ons. Do a `GET` request to this resource, with `?limit=0` added to indicate that you don't actually want a full list of add-ons (`https://marketplace.atlassian.com/rest/2/addons?limit=0`) -- you will receive a JSON response like this:

        ``` js
        {
          "_links": {
            "self": {
              "href": "/rest/2/addons"
            }, [...some more links that you can ignore...]
            "byKey": {
              "href": "/rest/2/addons/{addonKey}",
              "templated": true
            }
        }
        ```

    3.  Take the `href` property under `byKey`, and substitute the add-on key of your existing add-on for `{addonKey}` (for instance, `/rest/2/addons/my.excellent.addon`). Do a GET request to the resulting URL path, resulting in a JSON response like this:

        ``` js
        {
          "_links": {
            "self": {
              "href": "/rest/2/addons/my.excellent.addon"
            },
            "versions": {
              "href": "/rest/2/addons/my.excellent.addon/versions"
            }
        }
        ```

    4.  Now, the `href` property under `versions` gives you the URL path for the resource you'll use to create the version.

    -   For `ADDON_KEY`, substitute the add-on key of your existing add-on
-   Authentication: Basic Authentication, with the username and password of your Atlassian Account
-   Content type: `application/json`
-   Request body: the JSON document from step 2

``` text
curl --basic --user MY_USERNAME \
   -H "Content-Type: application/json" \
   --data-binary @/path/to/my-json-document.json \
   https://marketplace.atlassian.com/rest/2/addons/my.excellent.addon/versions
```

#### Response

If successful, you will receive an <a href="https://httpstatuses.com/201" class="external-link">HTTP 201</a> status with no response body. The response will have a `Location` header whose value is the URL path of the newly created version resource (which you can do a GET request to if you want to see the current properties of the version on the server), such as this:

`Location: /rest/2/addons/my.excellent.addon/versions/build/100`

If the properties you specified for the version were invalid or it could not be created for any other reason, you will instead receive an <a href="https://httpstatuses.com/400" class="external-link">HTTP 400</a> status with a JSON error document like this:

``` js
{
  "errors": [
    {
      "path": "/buildNumber",
      "message": "Must be unique"
    }
  ]
}
```

## Approval for new versions

Note that Atlassian may need to approve the new version of your add-on if you are creating a public version. For example, approval is required if the add-on was previously using a different payment model or hosting option. See [Add-on approval guidelines] for more information.

  [Prerequisites]: #prerequisites
  [Step 1: Upload the installable artifact, if any]: #step-1-upload-the-installable-artifact-if-any
  [Request]: #request
  [Response]: #response
  [Step 2: Build a JSON document for the new version]: #step-2-build-a-json-document-for-the-new-version
  [Descriptions of version properties]: #descriptions-of-version-properties
  [Step 3: Post the new version to Marketplace]: #step-3-post-the-new-version-to-marketplace
  [1]: #1
  [2]: #2
  [Approval for new versions]: #approval-for-new-versions
  [tool for making HTTP requests]: /market/examples-of-api-usage-through-json-requests-39984261.html
  [Add-on approval guidelines]: https://developer.atlassian.com/display/MARKET/Add-on+approval+guidelines

