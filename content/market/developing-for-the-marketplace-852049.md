---
title: Developing for the Marketplace 852049
aliases:
    - /market/developing-for-the-marketplace-852049.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=852049
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=852049
confluence_id: 852049
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Developing for the Marketplace

You can develop add-ons with the [Atlassian SDK] for internal use, or sell them on the <a href="https://marketplace.atlassian.com/manage/plugins/create" class="external-link">Atlassian Marketplace</a>. 

If you publish your add-ons in the Marketplace, Atlassian customers can also access them inside their host application, via the <a href="https://confluence.atlassian.com/display/UPM/Universal+Plugin+Manager+Documentation" class="external-link">Universal Plugin Manager</a>. Inside their host application like JIRA or Confluence, customers can find new add-ons to buy and install; as well as manage and upgrade existing add-ons. 

For now, Atlassian products have three possible modes of deployment; two of which are locally-hosted by customers. [Server-product development documentation] provides general information on developing add-ons, and we also have [documentation for Data Center add-on development]. This section houses documentation specific to Marketplace development, like adding marketing resources or watching and rating controls.

 **Included in this section**

-   [Plugin metadata files used by UPM and Marketplace]
-   [Referencing visual assets in your add-on JAR]
-   [Adding watching and rating controls]
-   [Automatic Atlassian Connect add-on polling & versioning]
-   [Data Center add-on development][documentation for Data Center add-on development]
-   [Marketplace API]

### **Other relevant resources**

-   [Licensing your add-on]
-   [Branding your add-on]
-   [Atlassian SDK documentation][Atlassian SDK]
-   [Cloud development with Atlassian Connect]

 

 

  [Atlassian SDK]: https://developer.atlassian.com/display/DOCS/Getting+Started
  [Server-product development documentation]: https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
  [documentation for Data Center add-on development]: /market/data-center-add-on-development-28317214.html
  [Plugin metadata files used by UPM and Marketplace]: /market/plugin-metadata-files-used-by-upm-and-marketplace-852095.html
  [Referencing visual assets in your add-on JAR]: /market/referencing-visual-assets-in-your-add-on-jar-9699404.html
  [Adding watching and rating controls]: /market/adding-watching-and-rating-controls-16352197.html
  [Automatic Atlassian Connect add-on polling & versioning]: /market/26936649.html
  [Marketplace API]: /market/marketplace-api-39368006.html
  [Licensing your add-on]: https://developer.atlassian.com/x/SIDg
  [Branding your add-on]: /market/branding-your-add-on-9699545.html
  [Cloud development with Atlassian Connect]: https://developer.atlassian.com/static/connect/docs/index.html

