---
title: Alternate Deployment Model for Licensed Plugins 10421807
aliases:
    - /market/alternate-deployment-model-for-licensed-plugins-10421807.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10421807
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10421807
confluence_id: 10421807
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Alternate Deployment Model for Licensed Plugins

It is possible that your plugin will fail to start if the Licensing API (either License Storage plugin or UPM 2.x+) is not present at the time of your plugin's installation.

## What was supposed to happen?

If you followed the [licensing tutorial], you should have the following component in your `atlassian-plugin.xml`:

``` html
<component key="pluginLicenseStoragePluginInstaller" class="com.atlassian.upm.license.storage.lib.PluginLicenseStoragePluginInstaller"/>
```

This `component`, when initialized, installs and/or enables the License Storage plugin (if needed) that supports your plugin's licensing. The License Storage plugin allows your plugin to have the Licensing API available always, even for older product versions that do not come bundled with UPM 2.0.

## What went wrong?

Some plugins have components that use the Licensing API and get initialized before the `PluginLicenseStoragePluginInstaller` component has a chance to initialize. When this occurs, the first component (the one that uses the Licensing API) fails to initialize. It fails because it cannot resolve the Licensing API's classes, and as a result, the plugin fails to start up -- all *before* `PluginLicenseStoragePluginInstaller` has a chance to complete.

To determine whether or not your plugin has this problem, remove the following `bundledArtifact`:

``` html
<bundledArtifact>
    <groupId>com.atlassian.upm</groupId>
    <artifactId>plugin-license-storage-plugin</artifactId>
    <version>${upm.license.compatibility.version}</version>
</bundledArtifact>
```

## How do I work around this problem?

You can deploy your plugin as an [OBR (OSGi Bundle Repository)] file with the License Storage plugin provided as a dependency.

1.  Bump your `upm.license.compatibility.version` property to 2.2.2 or later.
2.  Add the following `pluginDependencies` section to your `maven-<productName>-plugin` configuration:

    ``` html
    <plugin>
        <groupId>com.atlassian.maven.plugins</groupId>
        <artifactId>maven-jira-plugin</artifactId>
        <version>${amps.version}</version>
        <extensions>true</extensions>
        <configuration>

            ...

            <!-- Specify what to bundle in the OBR -->
            <pluginDependencies>
                <pluginDependency>
                    <groupId>com.atlassian.upm</groupId>
                    <artifactId>plugin-license-storage-plugin</artifactId>
                </pluginDependency>
            </pluginDependencies>

            ...

        </configuration>
    </plugin>
    ```

3.  Delete the following four packages from your `<DynamicImport-Package>` tag. If these are the only four packages listed as dynamic import packages, you should remove the entire `<DynamicImport-Package>` tag.

    ``` javascript
    com.atlassian.upm.api.license;version="2.0.1",
    com.atlassian.upm.api.license.entity;version="2.0.1",
    com.atlassian.upm.api.util;version="2.0.1",
    com.atlassian.upm.license.storage.plugin;version="${upm.license.compatibility.version}"
    ```

    If you specify `<Import-Package>` statements, you should add these four packages there.

4.  After rebuilding your plugin, distribute the `<artifactId>-<versionId>.obr` artifact instead of the `<artifactId>-<versionId>.jar` artifact.

You can verify the fix by starting up a fresh host product instance (without the License Storage plugin or UPM 2.x+ being installed). Then install your generated `.obr` artifact in the Universal Plugin Manager.

  [licensing tutorial]: /market/8947417.html
  [OBR (OSGi Bundle Repository)]: https://developer.atlassian.com/display/DOCS/Bundling+extra+dependencies+in+an+OBR

