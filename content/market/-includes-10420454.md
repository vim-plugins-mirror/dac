---
title: Includes 10420454
aliases:
    - /market/-includes-10420454.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420454
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420454
confluence_id: 10420454
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Includes

-   [\_License report]
-   [\_Revenue share example]
-   [\_Sales Report]
-   [\_Sizes Marketing]
-   [\_Feedback Developer]
-   [\_Marketing Requests]
-   [\_New Branding 2013]
-   [\_atlassian-plugin-marketing.xml]
-   [\_atlassian-marketing-plugin.xml branding]
-   [\_atlassian-plugin.xml branding]
-   [\_Developer documentation]
-   [\_Buttons]
-   [\_Approval]
-   [\_Branding 2013]
-   [\_Criteria]
-   [\_deployable]
-   [\_Resize warning]

  [\_License report]: /market/-license-report-10420478.html
  [\_Revenue share example]: /market/-revenue-share-example-10420459.html
  [\_Sales Report]: /market/-sales-report-10420475.html
  [\_Sizes Marketing]: /market/-sizes-marketing-11305282.html
  [\_Feedback Developer]: /market/-feedback-developer-11927707.html
  [\_Marketing Requests]: /market/-marketing-requests-11927920.html
  [\_New Branding 2013]: /market/-new-branding-2013-23298719.html
  [\_atlassian-plugin-marketing.xml]: /market/-atlassian-plugin-marketing.xml-24805547.html
  [\_atlassian-marketing-plugin.xml branding]: /market/-atlassian-marketing-plugin.xml-branding-24805599.html
  [\_atlassian-plugin.xml branding]: /market/-atlassian-plugin.xml-branding-24805609.html
  [\_Developer documentation]: /market/-developer-documentation-26935583.html
  [\_Buttons]: /market/-buttons-27557914.html
  [\_Approval]: /market/-approval-10421860.html
  [\_Branding 2013]: /market/-branding-2013-23298335.html
  [\_Criteria]: /market/-criteria-10421170.html
  [\_deployable]: /market/-deployable-10420847.html
  [\_Resize warning]: /market/-resize-warning-10421020.html

