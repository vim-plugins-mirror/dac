---
title: Referencing Visual Assets in Your Add On Jar 9699404
aliases:
    - /market/referencing-visual-assets-in-your-add-on-jar-9699404.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=9699404
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=9699404
confluence_id: 9699404
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Referencing visual assets in your add-on JAR

Add-ons listed in the Atlassian Marketplace require visual marketing assets like a logo and screenshots. Customers in the Marketplace use these assets to quickly assess what your add-on does, and learn how it interacts with Atlassian host applications. You can package visual marketing assets in your JAR. You should place these images in an `images` directory. Then, you can reference these assets in a file titled `atlassian-plugin-marketing.xml`. You can avoid manually filling out many visual marketing fields when you include a `atlassian-plugin-marketing.xml` in your JAR submission to the Atlassian Marketplace.

This page describes how to reference visual marketing assets in a file you can create, called `atlassian-plugin-marketing.xml.`

## Declaring marketing assets in `atlassian-plugin-marketing.xml`

This section describes how to reference your visual assets in a separate `atlassian-plugin-marketing.xml` file. The Atlassian Marketplace automatically detects your visual assets, and matches them to corresponding highlight and screenshot fields in the **Create add-on** form. Assets referenced in this file are only used on the Atlassian Marketplace.

**Important: **

-   Name your marketing descriptor  **`atlassian-plugin-marketing.xml`** .
-   Store this file under  **`src/main/resources`**  at the same level with the regular descriptor file, `atlassian-plugin.xml`.
-   Keep your images in an  **`images`**  directory within the JAR. 

The following is a table describing the visual assets you can describe in this file. 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Asset reference</th>
<th>Tag</th>
<th>Size of asset</th>
<th>Description of asset</th>
<th>Example reference to asset</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Add-on logo</td>
<td><code>logo</code></td>
<td>144 x 144px PNG/JPG</td>
<td>Reference your add-on logo. Use transparent or bounded, chiclet-style backgrounds. Marketplace automatically creates an add-on icon from your logo.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;logo image=&quot;images/01_logo.jpg&quot;/&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Add-on banner</td>
<td><code>banner</code></td>
<td>1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</td>
<td>Include your add-on name, your vendor name, and brief text about your add-on's functionality. Users see this banner displayed in the UPM when they browse the Marketplace from their Atlassian host product.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;banner image=&quot;images/02_banner.jpg&quot;/&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Highlight images</td>
<td><code>highlight</code></td>
<td><p>Full-size highlight:</p>
<p>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</p>
<p>Cropped highlight:</p>
<p>580 x 330px PNG/JPG</p></td>
<td><p><strong>Full-size:</strong> Illustrate your highlight with a screenshot. High-definition display screenshots encouraged.</p>
<p><strong>Cropped:</strong> Provide a cropped version of your screenshot. This image is displayed prominently on the add-on details page.</p></td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;highlights&gt;
        &lt;highlight image=&quot;images/03_highlight1_ss.jpg&quot; cropped=&quot;images/04_highlight1_cropped.jpg&quot;/&gt;
        &lt;highlight image=&quot;images/05_highlight2_ss.jpg&quot; cropped=&quot;images/06_highlight2_cropped.jpg&quot;/&gt;
        &lt;highlight image=&quot;images/07_highlight3_ss.jpg&quot; cropped=&quot;images/08_highlight3_cropped.jpg&quot;/&gt;
      &lt;/highlights&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Additional screenshots</td>
<td><code>screenshot</code></td>
<td>1840 x 900px PNG/JPG or 920 x 450px PNG/JPG for standard images</td>
<td>Provide additional screenshots to help illustrate your add-on's capabilities.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>   &lt;screenshots&gt;
        &lt;screenshot image=&quot;images/09_addl_ss1.jpg&quot;/&gt;
        &lt;screenshot image=&quot;images/10_addl_ss2.jpg&quot;/&gt;
        &lt;screenshot image=&quot;images/11_addl_ss3.jpg&quot;/&gt;
      &lt;/screenshots&gt;</code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

``` javascript
<atlassian-plugin-marketing>
       
      <!--Describe names and versions of compatible applications -->
      <compatibility>
        <product name="jira" min="4.0" max="4.4.1"/>
        <product name="bamboo" min="3.0" max="3.1"/>
      </compatibility>
       
      <!-- Describe your add-on logo and banner. The banner is only displayed in the UPM. -->
      <logo image="images/01_logo.jpg"/>
      <banner image="images/02_banner.jpg"/>
       
      <!-- Describe highlight and cropped highlight image assets, in order. -->
      <highlights>
        <highlight image="images/03_highlight1_ss.jpg" cropped="images/04_highlight1_cropped.jpg"/>
        <highlight image="images/05_highlight2_ss.jpg" cropped="images/06_highlight2_cropped.jpg"/>
        <highlight image="images/07_highlight3_ss.jpg" cropped="images/08_highlight3_cropped.jpg"/>
      </highlights>
       
      <!-- Describe additional screenshots you'd like listed on your add-on listing. -->
      <screenshots>
        <screenshot image="images/09_addl_ss1.jpg"/>
        <screenshot image="images/10_addl_ss2.jpg"/>
        <screenshot image="images/11_addl_ss3.jpg"/>
      </screenshots>
  
</atlassian-plugin-marketing>
```

## UPM visual marketing materials

The regular descriptor file, `atlassian-plugin.xml`, references visual assets used in UPM. These assets overlap with the Atlassian Marketplace marketing materials. This file should include the following asset references inside the `plugin-info` tag: 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th>Asset</th>
<th>Tag</th>
<th>Size of asset</th>
<th>Description of asset</th>
<th>Example reference to asset</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Vendor name &amp; URL</td>
<td><code>vendor</code></td>
<td><p>Vendor names can be up to 40 characters.</p>
<p> </p></td>
<td><p>Use your company name or, if an individual, your own name. Be mindful of trademark and copyright guidelines. Short names are preferred.</p></td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>&lt;vendor name=&quot;Atlassian Plugin Generator&quot; url=&quot;http://example.com&quot;/&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Vendor logo</td>
<td><code>param name=&quot;vendor-icon&quot;</code></td>
<td>72x72px PNG/JPG/GIF</td>
<td>Reference a large, clear, front-facing image. Your logo should be relevant to your vendor name. This appears on your vendor page in the Marketplace.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>&lt;param name=&quot;vendor-logo&quot;&gt;images/vendorLogo.jpg&lt;/param&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Vendor icon</td>
<td><code>param name=&quot;vendor-icon&quot;</code></td>
<td>16x16px PNG/JPG/GIF</td>
<td>Reference a smaller version of your vendor logo. This appears in the sidebar and top-ranked lists. If your logo doesn't scale down well, create an additional thumbnail image. In the future, your logo may be scaled to be your vendor icon.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>&lt;param name=&quot;vendor-icon&quot;&gt;images/vendorIcon.jpg&lt;/param&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="even">
<td>Add-on logo</td>
<td><code>param name=&quot;plugin-logo&quot;</code></td>
<td>144 x 144px PNG/JPG</td>
<td>Reference logo with a transparent or bounded, chiclet-style background. Marketplace automatically creates an add-on icon from your logo.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>&lt;param name=&quot;plugin-logo&quot;&gt;images/pluginLogo.jpg&lt;/param&gt;</code></pre>
</div>
</div></td>
</tr>
<tr class="odd">
<td>Add-on banner</td>
<td><code>param name=&quot;plugin-banner&quot;</code></td>
<td>1120 x 548px PNG/JPG for high-resolution images or 560 x 274px PNG/JPG for standard images</td>
<td>Include your add-on name, your vendor name, and brief text about your add-on's functionality. Users see this banner displayed in the UPM when they browse the Marketplace from their Atlassian host product.</td>
<td><div class="code pdl panel" style="border-width: 1px;">
<div class="codeContent pdl panelContent">
<pre class="javascript" style="font-size:12px;"><code>&lt;param name=&quot;plugin-banner&quot;&gt;images/pluginBanner.jpg&lt;/param&gt;</code></pre>
</div>
</div></td>
</tr>
</tbody>
</table>

Here's the full `plugin-info` section in the descriptor:

``` javascript
      <plugin-info>
        <description>Description of Example Marketing for Confluence</description>
        <version>1.0</version>
        <vendor name="Your Name Here" url="http://example.com"/>
        
        <param name="atlassian-licensing-enabled">true</param>
        <param name="plugin-icon">images/pluginIcon.jpg</param>
        <param name="plugin-banner">images/pluginBanner.jpg</param>
        <param name="vendor-logo">images/vendorLogo.jpg</param>
      </plugin-info>
```

Example JAR

If you'd like to view an example, you can unpack the following JAR to see how to package assets with your add-on. The add-on example is installable in UPM, but only serves to display how visual assets are described in the `atlassian-plugin-marketing.xml` file.

1.  Download the following JAR.
    **[example-add-on-marketing-confluence.jar]**
2.  Open a terminal window. 
3.  Extract the JAR contents.

        tar -xvf example-add-on-marketing-confluence.jar

    Your terminal displays the assets packaged with the JAR:

        x META-INF/MANIFEST.MF
        x atlassian-plugin.xml
        x atlassian-plugin-marketing.xml
        x images/11_addl_ss3.jpg
        x images/10_addl_ss2.jpg
        x images/09_addl_ss1.jpg
        x images/08_highlight3_cropped.jpg
        x images/07_highlight3_ss.jpg
        x images/06_highlight2_cropped.jpg
        x images/05_highlight2_ss.jpg
        x images/04_highlight1_cropped.jpg
        x images/03_highlight1_ss.jpg
        x images/02_banner.jpg
        x images/01_logo.jpg

4.  Open the JAR in your favorite editor or file viewer. 

5.  Examine the `atlassian-plugin-marketing.xml` marketing descriptor file.

  [example-add-on-marketing-confluence.jar]: attachments/9699404/24969419.jar

