---
title: Listing Step By Step 10420373
aliases:
    - /market/listing-step-by-step-10420373.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=10420373
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=10420373
confluence_id: 10420373
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Listing step-by-step

You've made it through the hard part, and developed your add-on. Now, you're ready to list your add-on in the Marketplace. This guide outlines how to list your add-on in the Marketplace, and ensure it gets approved. 

 

Skip ahead to any section: 

-   [Add a cloud version your existing server add-on]
-   [Add the Atlassian plugin licensing API to your add-on]
-   [Create your vendor profile in the Marketplace]
-   [Prepare your branding & marketing materials]
-   [Test your work]
-   [Submit your add-on for approval]
-   [Market your add-on]
-   [Make money]

 

 

 

 

## Add a cloud version your existing server add-on

If you already offer a Server add-on and you're listing the Cloud version of the same add-on, you should list them together (the inverse is also true). Each listing on Marketplace allows you to combine Server and Cloud versions with the same or different assets. This takes advantage of the SEO, ratings and reviews of your existing listing.  Your combined listing also looks more complete because both "Server" and "Cloud" models the listing display an available product.
To list your Server and Cloud add-on together, go to your current listing (i.e., Server) and click "Add Version", then submit your opposite deployment (i.e., Cloud) version. **You must use the same plugin key for both add-on types.**

## Add the Atlassian plugin licensing API to your add-on

Licensing controls let customers access your add-on from their Atlassian host product. Paid-via-Atlassian add-ons include the Atlassian plugin licensing API in order to be listed in the Marketplace.

**[Here's a tutorial to help you set this up]**.

## Create your vendor profile in the Marketplace

Every add-on is associated with a vendor profile. A vendor profile is a business entity, like Adaptivist or John Smith. Your add-on code references your vendor name, which is displayed to end users in the UPM and in the Marketplace. You can create a vendor profile from scratch on the Atlassian Marketplace or you can associate yourself with an existing vendor.

1.  Visit **<a href="https://marketplace.atlassian.com/" class="uri" class="external-link">https://marketplace.atlassian.com/</a>**
2.  Click **Login** or **Create account**.** **
3.  Log in with your Atlassian account credentials or create your account using your email address. 
4.  Click ****Manage listings****
5.  Click **<a href="https://marketplace.atlassian.com/manage/vendor/create" class="external-link">Create add-on</a>** and register your organization and contact details.
6.  Click **Create vendor**. 

## Prepare your branding & marketing materials

Looks count when it comes to selling add-ons. Atlassian requires that your add-on have the required branding materials. Some of these materials you place with your source code in your add-on JAR. Other materials you upload on the submission form when when you list your add-on.

1.  Assemble required branding materials. 
    [Check out our branding requirements here].
2.  Include your branding assets with your JAR or add-on source code.
    Learn how to [declare branding assets with your JAR]. 
3.  Include your vendor name, add-on description, and version information.
    See [how to reference information]in the `pom.xml` and descriptor file.

## Test your work

Test your add-on in the Atlassian product or products that your add-on works with.

1.  Verify that licensing works with extra [Timebomb Licenses for Testing].
2.  Ensure that your add-on adheres to all the [Marketplace approval guidelines].

## Submit your add-on for approval

Now you can submit your add-on for approval from the Marketplace team.

1.  Log into the Atlassian Marketplace.
    Click **Login** from **<a href="https://marketplace.atlassian.com/" class="uri" class="external-link">https://marketplace.atlassian.com/</a>.**
    Ensure you log into the vendor profile you created in step 2.
2.  Click **Manage listings** from the header.
3.  Fill out the submission form, following onscreen instructions.
4.  Accept the <a href="https://www.atlassian.com/licensing/marketplace/publisheragreement.html" class="external-link">Atlassian Marketplace Vendor Agreement</a>.
5.  Submit your add-on for approval.
    For paid-via-Atlassian add-ons, supply your bank account details to receive remittances. This includes your bank name, address, account numbers, tax ID, and other information.
6.  We'll email you to let you know when your add-on is approved!
    You can expect a response in 3-5 business days.

## Market your add-on

After you receive approval from Atlassian, we recommend double-checking your add-on details page.

1.  Review your listing and make sure everything looks like you expected.
2.  Ensure you can install your add-on from the Marketplace, and that links and functionality are intact.
3.  Double-check that your vendor profile financial information is complete and correct.
4.  Promote your add-on and [make the most of marketing].

## Make money

Our systems record each add-on evaluation or trial of your add-on. As these evaluations progress, our automated system sends emails to remind customers that their evaluation ends soon. These emails also prompt users to purchase a full license. Then, watch the Atlassian system work for you via automated reports. These reports provide daily and monthly sales and license reports.

1.  Read up on [sales and renewals for paid-via-Atlassian listings].
2.  View your [reports for paid-via-Atlassian Listings].

  [Add a cloud version your existing server add-on]: #add-a-cloud-version-your-existing-server-add-on
  [Add the Atlassian plugin licensing API to your add-on]: #add-the-atlassian-plugin-licensing-api-to-your-add-on
  [Create your vendor profile in the Marketplace]: #create-your-vendor-profile-in-the-marketplace
  [Prepare your branding & marketing materials]: #prepare-your-branding-&-marketing-materials
  [Test your work]: #test-your-work
  [Submit your add-on for approval]: #submit-your-add-on-for-approval
  [Market your add-on]: #market-your-add-on
  [Make money]: #make-money
  [Here's a tutorial to help you set this up]: https://developer.atlassian.com/x/poJjAQ
  [Check out our branding requirements here]: /market/branding-your-add-on-9699545.html
  [declare branding assets with your JAR]: https://developer.atlassian.com/x/TACU
  [how to reference information]: https://developer.atlassian.com/x/fwAN
  [Timebomb Licenses for Testing]: /market/timebomb-licenses-for-testing-8946477.html
  [Marketplace approval guidelines]: /market/add-on-approval-guidelines-10421849.html
  [make the most of marketing]: /market/making-the-most-of-marketing-9699543.html
  [sales and renewals for paid-via-Atlassian listings]: /market/sales-and-renewals-email-campaigns-10420347.html
  [reports for paid-via-Atlassian Listings]: /market/reports-for-paid-via-atlassian-listings-9699530.html

