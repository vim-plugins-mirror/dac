---
title: Add On Licensing for Developers 14712904
aliases:
    - /market/add-on-licensing-for-developers-14712904.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=14712904
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=14712904
confluence_id: 14712904
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Add-on licensing for developers

To create paid-via-Atlassian add-ons for sale on Atlassian Marketplace, you implement licensing features in the add-on that control its use in the Atlassian host application. In the host application, the Universal Plugin Manager (UPM) serves as the enforcement point that checks the validity of add-on licenses. The UPM provides an API that add-ons use to access the Atlassian licensing system for its license checks.

The following topics describe how to use Atlassian add-on licensing features.

-   [Tutorial: Adding licensing support to your add-on]
-   [Timebomb Licenses for Testing]
-   [License Validation Rules]
-   [Converting proprietary licenses into Marketplace licenses]
-   [Licensing and Paid-via-Atlassian Listings]
-   [Licensing API Overview]
-   [Tutorial: Omit web-resources when your plugin is unlicensed]

##### *
*

  [Tutorial: Adding licensing support to your add-on]: /market/23298726.html
  [Timebomb Licenses for Testing]: /market/timebomb-licenses-for-testing-8946477.html
  [License Validation Rules]: /market/license-validation-rules-8946470.html
  [Converting proprietary licenses into Marketplace licenses]: /market/converting-proprietary-licenses-into-marketplace-licenses-10421889.html
  [Licensing and Paid-via-Atlassian Listings]: /market/licensing-and-paid-via-atlassian-listings-13632223.html
  [Licensing API Overview]: /market/licensing-api-overview-8947414.html
  [Tutorial: Omit web-resources when your plugin is unlicensed]: /market/33741755.html

