---
title: Get Add On Details From Java 39984291
aliases:
    - /market/get-add-on-details-from-java-39984291.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39984291
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39984291
confluence_id: 39984291
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Get add-on details from Java

The [Marketplace API Java client] lets you get information about an add-on through the `Addons` API.

For a stand-alone demo, see `GetAddon.java` in the <a href="https://bitbucket.org/atlassian_tutorial/marketplace-v2-api-tutorials" class="external-link">Marketplace API Tutorials</a> repository.

-   [Prerequisites]
-   [Query a public add-on]
-   [Include version data]

## Prerequisites

You will need:

-   A Java project that uses the `marketplace-client-java` library
-   The add-on key of an existing add-on listing on Marketplace
-   A `MarketplaceClient` object

## Query a public add-on

The `getByKey` method of the `Addons` API queries the details of an add-on given its add-on key. It returns an `Option` containing an `Addon` object, or `Option.none()` if the add-on key was not found.

``` java
    Option<Addon> result = client.addons().getByKey("my.excellent.addon", AddonQuery.any());
    // Option implements Iterable, so we can use "for" to execute this code only if it has a value    
    for (Addon a: result) {
        System.out.println("I found the add-on; its name is " + a.getName());
    }
    if (!result.isDefined()) {
        System.out.println("not found");
    }
```

## Include version data

Note that we specified the default option (`AddonQuery.any()`) in the example above. One way to vary the query is to ask for the current add-on version at the same time:

``` java
    AddonQuery query = AddonQuery.builder().
        withVersion(true).
        build();
    Option<Addon> result = client.addons().getByKey("my.excellent.addon", query);
    for (Addon a: result) {
        System.out.println("I found the add-on; its name is " + a.getName());
        for (AddonVersion v: a.getVersion()) {
            System.out.println("And the latest version is " + v.getName());
        }
    }
```

If you specify any other criteria in the `AddonQuery`, you will get the latest add-on version that matches those criteria. If there is no such version, the query will return nothing (not even the add-on). In this example, we are asking for the latest version that is compatible with Cloud:

``` java
    AddonQuery query = AddonQuery.builder().
        hosting(Option.some(HostingType.CLOUD)).
        withVersion(true).
        build();
    Option<Addon> result = client.addons().getByKey("my.excellent.addon", query);
    for (Addon a: result) {
        System.out.println("I found the add-on; its name is " + a.getName());
        for (AddonVersion v: a.getVersion()) {
            System.out.println("And the latest Cloud version is " + v.getName());
        }
    }
```

  [Marketplace API Java client]: /market/using-the-marketplace-api-java-client-39984232.html
  [Prerequisites]: #prerequisites
  [Query a public add-on]: #query-a-public-add-on
  [Include version data]: #include-version-data

