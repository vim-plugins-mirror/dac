---
title: Promotions for Add Ons 25395372
aliases:
    - /market/promotions-for-add-ons-25395372.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=25395372
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=25395372
confluence_id: 25395372
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Promotions for add-ons

You can create promotions for paid-via-Atlassian add-ons you sell for downloaded instances of Atlassian products. The Marketplace provides a customer-friendly URL for users to apply and access your promotion. This is the only way for customers to access and apply the discount - promotions aren't outwardly visible on the Marketplace site. Customers accessing your promotion URL are directed to a specialized checkout page, detailing the sale price of the add-on.  

What's here:

-   [Promotion eligibility]
-   [Customer promotion experience]
-   [Creating a promotion]
-   [Modifying a promotion]
-   [Ending a promotion early]
-   [Promotions and sales reports]

## Promotion eligibility

At this time, promotions are available for paid-via-Atlassian add-ons compatible with server versions of Atlassian products. The table below shows a breakdown of which license types, sale types and hosting models are eligible for promotions.

Customers can always use promotions for any eligible license or sale type. For example, if you create a promotion for a JIRA add-on, you don't need to specify that it applies to new, renewal or upgrade purchases; it can automatically be used for any of these. There is no way for you to limit a promotion to one license or sale type.

A given promotion applies only to the initial transaction where it was used. For example, if a customer uses the promotion for a discounted first-time purchase of your add-on, the discount only applies to that purchase transaction. If the customer purchases additional maintenance as part of that transaction it will also be discounted. However, when the customer subsequently wants to renew maintenance, the promotion discount no longer applies and they'll need to pay full price for the renewal.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th>Condition</th>
<th>Eligibility</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Payment model</td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Paid via Atlassian</p>
<p><img src="/market/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /> Paid via vendor</p></td>
</tr>
<tr class="even">
<td>License type</td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Commercial license</p>
<p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Academic license*</p></td>
</tr>
<tr class="odd">
<td>Sale type</td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> New license</p>
<p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Renewal of existing license*</p>
<p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Upgrade of existing license*</p></td>
</tr>
<tr class="even">
<td>Hosting model</td>
<td><p><img src="/market/images/icons/emoticons/check.png" alt="(tick)" class="emoticon-tick" /> Server products</p>
<p><img src="/market/images/icons/emoticons/error.png" alt="(error)" class="emoticon-cross" /> Cloud products</p></td>
</tr>
</tbody>
</table>

\* Applies to JIRA Server, Confluence Server, Bitbucket Server and FishEye/Crucible add-ons only. Promotions for Bamboo Server add-ons cannot be used for academic licenses, renewals or upgrades.

## Customer promotion experience

To distribute promotions, provide a URL from the Marketplace to your customer(s) directly. When customers follow your link, your add-on is added to their cart with the promotion applied. The promotion URL is automatically created when you complete the steps in the section ahead. Promo links are always ** `https://promo.atlassian.com` **appended with a six-digit alphanumeric code.

Promo code example

**<a href="https://promo.atlassian.com/9SO6RI" class="uri" class="external-link">https://promo.atlassian.com/9SO6RI</a>** 

## Creating a promotion

You can create any number of promotions simultaneously for your paid-via-Atlassian add-ons. You can create single-use promotions, meaning that the URLs can only be used by one customer. Accordingly, single-use promotions should only be shared with one customer since all URLs are unique. You can also create multi-use promotions. Multi-use promotions use the same URL and can be used by as many customers as you permit. You can also create promotions for multiple add-ons, called bundled promotions. All add-ons must be for the same application and be paid-via-Atlassian to qualify, and all add-ons in the bundle need to be purchased for the customer to receive the discount.

Here's how to create a promotion: 

1.  Log into your account at  **<a href="https://marketplace.atlassian.com/" class="uri" class="external-link">https://marketplace.atlassian.com/</a>**
2.  Navigate to your **Manage add-on** page. 
    You can access this page when you click **Manage add-on** from your add-on details page.
    <img src="/market/attachments/25395372/28410977.png" width="500" />
3.  Click **Promotions**. 
4.  Click **Create new promotion**. 
    <img src="/market/attachments/25395372/28410976.png" width="500" />
5.  Complete each section and click **Next** to navigate forward. 
6.  Click **Submit** on the *Confirmation *screen to create your promotion.
7.  Copy the URL from the subsequent promotions page.
    <img src="/market/attachments/25395372/28410979.png" width="500" height="329" />
    Use this URL to share with your customers. Customers can follow this link and have the promotion automatically applied to their cart.

## Modifying a promotion

You can edit a live promotion any time. Promotions that have already ended can't be edited retroactively, of course, but ongoing promotions permit the following changes:

-   **Promotion name**: You can rename your promotion anything you like.
-   **Maximum number of uses**: For multi-use sharable promotions, you can raise the number of times your promotion can be used. 
-   **Extend the end date**: You can extend a promotion any time. Promotions are intentionally designed not to be shortened since this is a confusing experience for customers.

To edit a promotion, navigate to your *Manage vendor *page. From here, click the pencil icon to edit a promotion.

## Ending a promotion early

The only way to end a promotion early is to <a href="https://ecosystem.atlassian.net/browse/AMKT/" class="external-link">file a ticket in our tracker</a>. We advise only terminating promotions only when absolutely necessary, since this can result in a suboptimal customer experience.

## Promotions and sales reports

When a customer uses a promotion and finalizes a sale, you'll see this sale reflected in your [sales reports].

There are two scenarios when promotions appear used, but aren't immediately visible in your sales reports: 

1.  A promotion code is considered used when it's included in a sales quote. If the customer moves forward with the sale, the license and sale are accordingly shown in your sales reports. Quotes are valid for 30 days, and if the customer *doesn't* move forward with the sale the promotion will show as unused after this time. 
2.  Alternatively, if an order is flagged for review - for example, if our system detects a potential error in the order - we review the order before the sale is finalized. Once the sale is finalized, it's reflected in your sales report. 

Also note that 100% off promotions result in a $0 "sale" and are therefore not reflected in sales reports.

  [Promotion eligibility]: #promotion-eligibility
  [Customer promotion experience]: #customer-promotion-experience
  [Creating a promotion]: #creating-a-promotion
  [Modifying a promotion]: #modifying-a-promotion
  [Ending a promotion early]: #ending-a-promotion-early
  [Promotions and sales reports]: #promotions-and-sales-reports
  [sales reports]: /market/sales-reports-for-paid-via-atlassian-listings-13632227.html

