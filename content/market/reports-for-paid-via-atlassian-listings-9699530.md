---
title: Reports for Paid via Atlassian Listings 9699530
aliases:
    - /market/reports-for-paid-via-atlassian-listings-9699530.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=9699530
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=9699530
confluence_id: 9699530
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : Reports for Paid-via-Atlassian Listings

If your add-on is paid-via-Atlassian, Atlassian provides you data on your customers and sales. The Atlassian Marketplace EULA (Terms of Use) for customers specifies that Atlassian may forward customer data to you, the third-party vendor.

See [Accessing Sales Reports with the REST API] for reporting updates

 

  [Accessing Sales Reports with the REST API]: /market/accessing-sales-reports-with-the-rest-api-13633048.html

