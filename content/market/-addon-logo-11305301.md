---
title: Addon Logo 11305301
aliases:
    - /market/-addon-logo-11305301.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=11305301
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=11305301
confluence_id: 11305301
platform:
product:
category:
subcategory:
---
# Atlassian Marketplace : \_Addon Logo

144 x 144px PNG/JPG



