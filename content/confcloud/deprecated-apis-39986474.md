---
title: Deprecated APIs 39986474
aliases:
    - /confcloud/deprecated-apis-39986474.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39986474
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39986474
confluence_id: 39986474
platform:
product:
category:
subcategory:
---
# Confluence Connect : Deprecated APIs



