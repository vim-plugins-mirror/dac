---
title: Remote API Specification for Pdf Export 39985853
aliases:
    - /confcloud/remote-api-specification-for-pdf-export-39985853.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985853
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985853
confluence_id: 39985853
platform:
product:
category:
subcategory:
---
# Confluence Connect : Remote API Specification for PDF Export

Confluence has a new [REST API] that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>Confluence 3.0 and later</p></td>
</tr>
</tbody>
</table>

 

In Confluence 3.0, we moved the PDF export engine to a Confluence plugin. Therefore, you can no longer use [Confluence's built-in remote API] to export a space as a PDF. There is a new replacement API that is part of the new PDF export Confluence plugin.

**On this page:**

-   [XML-RPC Information]
-   [SOAP Information]
    -   [Methods]

## XML-RPC Information

-   The URL for XML-RPC requests is http://confluence.example.com/rpc/xmlrpc 
-   All XML-RPC methods must be prefixed by `pdfexport.`

## SOAP Information

To find out more about the SOAP API, simply point your SOAP 'stub generator' at the WSDL file, located at http://confluence.example.com/rpc/soap-axis/pdfexport?wsdl

For reference, the pdfexport WSDL file is [here].

### Methods

-   String login(String username, String password) - log in a user. Returns a String authentication token to be passed as authentication to all other remote calls. It's not bulletproof auth, but it will do for now. Must be called before any other method in a 'remote conversation'. From 1.3 onwards, you can supply an empty string as the token to be treated as being the anonymous user.
-   public String exportSpace(String token, String spaceKey) - exports the entire space as a PDF. Returns a url to download the exported PDF. Depending on how you have Confluence set up, this URL may require you to authenticate with Confluence. Note that this will be normal Confluence authentication, not the token based authentication that the web service uses.

  [REST API]: /confcloud/confluence-rest-api-39985291.html
  [Confluence's built-in remote API]: /confcloud/confluence-xml-rpc-and-soap-apis-39985844.html
  [XML-RPC Information]: #xml-rpc-information
  [SOAP Information]: #soap-information
  [Methods]: #methods
  [here]: https://developer.atlassian.com/rpc/soap-axis/pdfexport?wsdl

