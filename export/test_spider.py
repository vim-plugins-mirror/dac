import spider

sample_html = """
    <html>
    <head>
    <title>test data</title>
    </head>
    <body>
    <h1>big title</h1>

    <li class="first">
        <span><a href="index.html">JIRA Cloud</a></span>
    </li>
                            <li>
        <span><a href="JIRA-Cloud-for-Developers_39375886.html">JIRA Cloud for Developers</a></span>
    </li>

    <div class="aui-page-panel-inner" id="feedback-block">
        Don't need this stuff!
    </div>
    </body>
    </html>
"""


def test_extract_valid_links():
    s = spider.Spider("host", "", ".html", 0)
    links = s.extract_valid_links_from_html(sample_html)
    assert links[0] == "index.html"
    assert links[1] == "JIRA-Cloud-for-Developers_39375886.html"
    assert len(links) == 2
