#!/bin/bash

set -ex

REPO=docker.atlassian.io/atlassian/dac-static
VERSION=`cat release.txt`   # Saved during build phase

docker build --tag ${REPO}:${VERSION} -f docker/micros-image.dockerfile .

docker push ${REPO}:${VERSION}
