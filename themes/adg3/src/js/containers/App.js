import React from 'react';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'

import rootReducer from '../reducers'
import AsyncUser from './AsyncUser'

const loggerMiddleware = createLogger()

export function configureStore(initialState) {
  return createStore(
    rootReducer,
    initialState,
    applyMiddleware(
      thunkMiddleware
//      loggerMiddleware
    )
  )
}

const store = configureStore()

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <div className="profile">
          <p className="title">
            <span className="aui-icon icon-success"></span>
          </p>
          <AsyncUser />
        </div>
      </Provider>
    );
  }
};

export default App
